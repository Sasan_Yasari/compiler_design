grammar X;

@header {
    import java.util.Vector;
    import java.util.Random;
    import java.util.ArrayList;
}

@members{
    X prog= new X();
    void print(String str){
        System.out.println(str);
    }
    String[] PB = new String[1000];
    int pc = 0;
    boolean isAccFull=false;
    int addToStack(Env cur, String varName, int pc){
        for(int i=0; i<cur.getSize(varName)/4; i++){
            if(isAccFull){
                PB[pc] = "sw \$a0, 0(\$sp)";
                PB[pc+1] = "addiu \$sp, \$sp, -4";
                pc += 2;
            }
            else
                isAccFull=true;
            PB[pc] = "lw \$a0, -"+((cur.getAdr(varName)+i)*4)+"(\$fp)";
            pc++;
        }
        return pc;
    }
    int addToStack(int val, int pc){
        if(isAccFull){
            PB[pc] = "sw \$a0, 0(\$sp)";
            PB[pc+1] = "addiu \$sp, \$sp, -4";
            pc += 2;
        }
        else
            isAccFull=true;
        PB[pc] = "li \$a0, "+val;
        pc++;
        return pc;
    }
    int addToStack(char c, int pc){
        if(isAccFull){
            PB[pc] = "sw \$a0, 0(\$sp)";
            PB[pc+1] = "addiu \$sp, \$sp, -4";
            pc += 2;
        }
        else
            isAccFull=true;
        PB[pc] = "li \$a0, \'"+c+"\'";
        pc++;    
        return pc;
    }
    String operationCommand(String op){
        if("+".equals(op))
            return "add \$a0, \$a0, \$t1";
        if("-".equals(op))
            return "sub \$a0, \$a0, \$t1";
        if("*".equals(op))
            return "mul \$a0, \$a0, \$t1";
        if("/".equals(op))
            return "div \$a0, \$t1, \$a0";//note it
        return "";
    }
    public static String labelGenerator(int len){
        final String tmp = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ ) 
           sb.append( tmp.charAt( rnd.nextInt(tmp.length()) ) );
        return sb.toString();
    }
    ArrayList<Integer> ifJmpadr = new ArrayList<Integer>();
    ArrayList<Integer> elseJmpadr = new ArrayList<Integer>();
    ArrayList<Integer> breakAdr = new ArrayList<Integer>();
    boolean isBreak = false;
}   

program
    :
    {
        Env top = prog.addEnv(null);
        PB[pc] = "main:";
        PB[pc+1] = "move \$fp \$sp";
        pc += 2;
    }
    ( fordecl a=';'
    | funcdef[top] 
    | typedef[top] ';'
    | vardef[true, top] ';')+ 
    {
        // prog.printEnvs();
        PB[pc] = "li \$v0, 10";
        PB[pc+1] = "syscall";
        pc += 2;
        for(String s : PB){
            if(s==null)
                break;
            print(s);
        }
    }
    ;
fordecl returns [Type retType, String name, ArrayList<Pair<String, Type>> argus]
    : ff=func_fordecl {$retType = $ff.retType; $name = $ff.name; $argus = $ff.argus;}
    | type_fordecl 
    ;
type_fordecl returns [String name]
    : 'typedef' ID 
    {
        $name = $ID.text;
    }
    ;
func_fordecl returns [Type retType, String name, ArrayList<Pair<String, Type>> argus]
    : t1=type func_name=ID '(' 
    {
        $retType = $t1.t;
        $name = $func_name.text;
    }
    ((n2=ID ':')? t2=type 
    {
        $argus = new ArrayList<Pair<String, Type>>();
        Pair<String, Type> newPair = new Pair($n2.text, $t2.t); 
        $argus.add(newPair);
    }
    (',' (n3=ID ':')? t3=type
    {
        Pair<String, Type> newPair2 = new Pair($n3.text, $t3.t); 
        $argus.add(newPair2);
    }
    )*)? ')' 
    ;
funcdef [Env top]
    locals [ArrayList<Pair<String, Type>> args]
    :t1=type func_name=ID '(' (('$')?n2=ID ':' t2=type 
    {
        $args = new ArrayList<Pair<String, Type>>();
        Pair<String, Type> newPair = new Pair($n2.text, $t2.t); 
        $args.add(newPair);
        $top.put($n2.text, $t2.t);
    }
    (',' ('$')?n3=ID ':' t3=type
    {
        Pair<String, Type> newPair2 = new Pair($n3.text, $t3.t); 
        $args.add(newPair2);
        $top.put($n3.text, $t3.t);
    }
    )*)? ')' block[$t1.t, $top]
    ;
typedef [Env top]
    : type_fordecl a='as' type 
    {
        $top.put($type_fordecl.name, $type.t);
    }
    ;
vardef [boolean isGlobal, Env top]
    : type n1=ID {$top.put($n1.text, $type.t);}
    ('=' e1=expr[$top])? (',' n2=ID{$top.put($n2.text, $type.t);} (a='=' e2=expr[$top])?)* 
    ;
type returns [Type t]
    locals [boolean flag, String res, Type temp, ArrayList<Type> types]
    : ID {$t = new Type($ID.text);} 
    | {$res = "";$t = new Type("list");} a='[' (n1=ID {$res = $n1.text;}':' )? t1=type 
    {
        $t.addType($res, $t1.t);
        $res = "";
    } 
    (b=',' (n2=ID {$res = $n2.text;}':')? t2=type 
    {
        $t.addType($res, $t2.t);
    })* ']'
    | 'list' '(' CONST_NUM ')' 'of' t1=type 
    {
        $t = new Type("list");
        $temp = $t1.t;
        for(int i=0;i<Integer.parseInt($CONST_NUM.text);i++)
            $t.addType("", $temp);
    }
    | a='<'(t1=type 
    {
    	$types = new ArrayList<>();
        $types.add(new Type($t1.t));
    }   
    ('*' t2=type {$types.add(new Type($t2.t));})*) 
        {
            $temp = new Type("->");
        }'->' 
        t3=type 
        {
            $t = new Type($t3.t);
            for(int i=0;i<$types.size();i++)
                $t.addType("", $types.get(i));
        }'>' 
    | 'string' '(' CONST_NUM ')' 
    {   
        $t = new Type("list");
        $temp = new Type("char");
        for(int i=0;i<Integer.parseInt($CONST_NUM.text);i++)
            $t.addType("", $temp);
    }
    | 'char' {$t = new Type("char");}
    | 'int' {$t = new Type("int");}
    | 'void' {$t = new Type("void");}
    ;
block [Type ret, Env prev]
    : '{' {Env top = prog.addEnv($prev);}(statement[$ret, top])* 
    {
        PB[pc] = "addiu \$sp, \$sp, "+top.offset;
        isBreak = false;
    }'}' 
    ;
statement [Type ret, Env top]
    : if_stm [$ret,$top]
    | other_stm [$ret, $top]
    ;
other_stm [Type ret, Env top]
    : vardef[false, $top] ';' 
    | expr[$top] ';'
    | return_stm[$ret, $top] ';'
    | block[$ret, $top]
    | while_stm[$ret, $top]
    | foreach_stm[$ret,prog.addEnv($top)] 
    | break_stm ';'
    ;
if_stm [Type ret, Env top] locals [ String label1, String label2]
    : 'if' '(' expr[$top] ')' 
    {
        isBreak = false;
        ifJmpadr.add(pc);
        pc++;
    }statement [$ret, $top]
    {
        $label1 = labelGenerator(3);
        PB[ifJmpadr.get(ifJmpadr.size()-1)] = "blez \$a0, " + $label1;
        ifJmpadr.remove(ifJmpadr.get(ifJmpadr.size()-1));
        PB[pc] = $label1+ ":";
        pc++;
    }
    | 'if' '(' expr[$top] ')' 
    {
        isBreak = false;
        ifJmpadr.add(pc);
        pc++;
    }
    if_stm_else[$ret,$top] 'else'
    {
        $label1 = labelGenerator(3); 
        PB[ifJmpadr.get(ifJmpadr.size()-1)] = "blez \$a0, " + $label1;
        ifJmpadr.remove(ifJmpadr.get(ifJmpadr.size()-1));
        elseJmpadr.add(pc) ; 
        PB[pc+1] = $label1+ ":";
        pc += 2;
        isBreak = false;
    } statement[$ret, $top]
    {
        $label2 = labelGenerator(3);
        PB[pc] = $label2+ ":";
        PB[elseJmpadr.get(elseJmpadr.size()-1)] = "j " + $label2;
        elseJmpadr.remove(elseJmpadr.get(elseJmpadr.size()-1));
        pc++;
    }
    ;
if_stm_else [Type ret, Env top] locals[String label1, String label2]
    : 'if' '(' expr[$top] ')' 
    {
        isBreak = false;
        ifJmpadr.add(pc);
        pc++;
    }
     if_stm_else[$ret,prog.addEnv($top)] 
      'else'
    {
        $label1 = labelGenerator(3); 
        PB[ifJmpadr.get(ifJmpadr.size()-1)] = "blez \$a0, " + $label1;
        ifJmpadr.remove(ifJmpadr.get(ifJmpadr.size()-1));
        elseJmpadr.add(pc) ; 
        PB[pc+1] = $label1+ ":";
        pc += 2;
        isBreak = false;
    } 
       if_stm_else[$ret,prog.addEnv($top)] 
    {
        $label2 = labelGenerator(3);
        PB[pc] = $label2+ ":";
        PB[elseJmpadr.get(elseJmpadr.size()-1)] = "j " + $label2;
        elseJmpadr.remove(elseJmpadr.get(elseJmpadr.size()-1));
        pc++;
    }
    | other_stm[$ret, $top]
    ;
expr [Env top] returns [String name, Type t, int num]
    : expr_assign[$top] 
    {        
        $name = $expr_assign.name;
        $t = $expr_assign.t;
        $num = $expr_assign.num;
        // System.out.println($num);
        // print($name);
    }
    ;
expr_assign [Env top] returns [String name, Type t, int num]
    : expr_or[$top] a='=' ea=expr_assign[$top]
    {
        $t = $expr_or.t;
        // pc = addToStack($top, $expr_assign.name, pc);
    }
    | expr_or[$top]  
    {
        $name = $expr_or.name;
        $t = $expr_or.t;
        $num = $expr_or.num;
    }
    ;
expr_or [Env top] returns [String name, Type t, int num]
    : expr_and[$top] expr_or_tmp[$top, $expr_and.num] 
    {
        $name = $expr_and.name;
        $t = $expr_and.t;
        $num = $expr_or_tmp.num;
    }
    ;
expr_or_tmp [Env top, int inh] returns [int num]
    : '||' expr_and[$top] 
    {
        PB[pc] = "lw \$t0, 4(\$sp)";
        PB[pc+1] ="or \$a0, \$a0, \$t0";
        PB[pc+2] ="addi \$sp, \$sp, 4";
        pc += 3;
        if($inh>0 || $expr_and.num>0)
            $num = 1;
        else
            $num = 0;
    } expr_or_tmp[$top, $num] 
    | {$num = $inh;}
    ;
expr_and [Env top] returns [String name, Type t, int num]
    : expr_eq[$top] expr_and_tmp[$top, $expr_eq.num] 
    {
        $name = $expr_eq.name;
        $t = $expr_eq.t;
        $num = $expr_and_tmp.num;
    }
    ;
expr_and_tmp [Env top, int inh] returns [int num]
    : '&&' expr_eq[$top] 
    {
        PB[pc] = "lw \$t0, 4(\$sp)";
        PB[pc+1] ="and \$a0, \$a0, \$t0";
        PB[pc+2] ="addi \$sp, \$sp, 4";
        pc += 3;
        if($inh>0 && $expr_eq.num>0)
            $num = 1;
        else
            $num = 0;
    }expr_and_tmp[$top, $num]
    |{$num = $inh;}
    ;
expr_eq [Env top] returns [String name, Type t, int num]
    : expr_cmp[$top] expr_eq_tmp[$top, $expr_cmp.num] 
    {
        $name = $expr_cmp.name; 
        $t = $expr_cmp.t;
        $num = $expr_eq_tmp.num;
    }
    ;
expr_eq_tmp [Env top, int inh] returns [int num]
    : op=('=='|'!=') expr_cmp[$top] 
    {
        if($op.text.equals("==")){
            $num = 1;
            PB[pc] = "lw \$t0, 4(\$sp)";
            PB[pc+1] = "sub \$a0, \$t0, \$a0";
            PB[pc+2] = "li \$t1, 1";
            PB[pc+3] = "move \$t2, \$a0";
            PB[pc+4] = "movz \$a0,\$t1, \$a0";
            PB[pc+5] = "movn \$a0,\$0, \$t2";
            PB[pc+6] = "addiu \$sp, \$sp, 4";
            pc += 7;
        }
        else{
            $num = 0;
            PB[pc] = "lw \$t0, 4(\$sp)";
            PB[pc+1] = "sub \$a0, \$t0, \$a0";
            PB[pc+2] = "li \$t1, 1";
            PB[pc+3] = "move \$t2, \$a0";
            PB[pc+4] = "movz \$a0,\$0, \$a0";
            PB[pc+5] = "movn \$a0,\$t1, \$t2";
            PB[pc+6] = "addiu \$sp, \$sp, 4";
            pc += 7;
        }
    }expr_eq_tmp[$top, $num]
    |{$num = $inh;}
    ;
expr_cmp [Env top] returns [String name, Type t, int num]
    : expr_add[$top] expr_cmp_tmp[$top, $expr_add.num] 
    {
        $name = $expr_add.name;
        $t = $expr_add.t;
        $num = $expr_cmp_tmp.num;
    }
    ;
expr_cmp_tmp[Env top, int inh] returns [int num]
    : op=('<'|'>') expr_add[$top] 
    {
        if($op.text.equals(">")){
            $num = 1;
            PB[pc] = "lw \$t0, 4(\$sp)";
            PB[pc+1] = "sub \$a0, \$t0, \$a0";
            PB[pc+2] = "slt \$a0, \$0, \$a0";
            PB[pc+3] = "addiu \$sp, \$sp, 4";
            pc += 4;
        }
        else{
            $num = 0;
            PB[pc] ="lw \$t0, 4(\$sp)";
            PB[pc+1] ="slt \$a0, \$t0, \$a0";
            PB[pc+2] = "addiu \$sp, \$sp, 4";
            pc += 3;
        }
    }expr_cmp_tmp[$top, $num]
    |{$num = $inh;}
    ;
expr_add [Env top] returns [String name, Type t, int num]
    : expr_mult[$top] expr_add_tmp[$top, $expr_mult.num] 
    {
        $name = $expr_mult.name;
        $t = $expr_mult.t;
        $num = $expr_add_tmp.num;
    }
    ;
expr_add_tmp [Env top, int inh] returns [int num]
    : op=('+'|'-') expr_mult[$top] 
    {
        if($op.text.equals("+")){
            $num = $inh + $expr_mult.num;
            PB[pc] = "lw \$t1, 4(\$sp)";
            PB[pc+1] = "add \$a0, \$a0, \$t1";
            PB[pc+2] = "addiu \$sp, \$sp, 4";
            pc += 3;
        }
        else{
            $num = $inh - $expr_mult.num;
            PB[pc] = "lw \$t1, 4(\$sp)";
            PB[pc+1] = "sub \$a0, \$a0, \$t1";
            PB[pc+2] = "addiu \$sp, \$sp, 4";
            pc += 3;
        }
    }expr_add_tmp[$top, $num]
    | {$num = $inh;}
    ;
expr_mult [Env top] returns [String name, Type t, int num]
    : expr_un[$top] expr_mult_tmp[$top, $expr_un.num]
    {
        $name = $expr_un.name;
        $t = $expr_un.t;
        $num = $expr_mult_tmp.num;
    } 
    ;
expr_mult_tmp [Env top, int inh] returns [int num]
    : op=('*'|'/') expr_un[$top] 
    {
        if($op.text.equals("*")){
            $num = $inh * $expr_un.num;
            PB[pc] = "lw \$t1, 4(\$sp)";
            PB[pc+1] = "mul \$a0, \$a0, \$t1";
            PB[pc+2] = "addiu \$sp, \$sp, 4";
            pc += 3;
        }
        else{
            $num = $inh / $expr_un.num;
            PB[pc] = "lw \$t1, 4(\$sp)";
            PB[pc+1] = "div \$a0, \$t1, \$a0";
            PB[pc+2] = "addiu \$sp, \$sp, 4";
            pc += 3;
        }
    }expr_mult_tmp[$top, $num]
    | {$num = $inh;}
    ;
expr_un [Env top] returns [String name, Type t, int num]
    : op=('!'|'-') eu=expr_un[$top] 
    {
        $t = $expr_un.t; 
        if($op.text.equals("!")){
            if($eu.num == 0){
                PB[pc] = "lw \$t1, 4(\$sp)";
                PB[pc+1] = "not \$a0, \$t1";
                pc += 2;
            }
            else{
                PB[pc] = "lw \$t1, 4(\$sp)";
                PB[pc+1] = "neg \$a0, \$a0, \$t1";
                pc += 2;
            }
        }
        else if($eu.num != 0)     
            $num = -$eu.num;
        else
            $num = 0;
    }
    | expr_mem[$top] 
    {
        $name = $expr_mem.name;
        $t = $expr_mem.t;
        $num = $expr_mem.num;
    }
    ;
expr_mem [Env top] returns [String name, Type t, int num] 
locals [boolean flag, Type temp]
    : expr_other[$top] expr_mem_tmp[$top] 
    {   
        $num = $expr_other.num;
        $flag = false;
        $name = $expr_mem_tmp.indexName; 
        $t = $expr_other.t;
        if($expr_mem_tmp.sharp){
            $t = $expr_other.t.t.get($expr_mem_tmp.num).getR();
        }
        else if($expr_mem_tmp.dot)
            $t = $expr_other.t.contain($expr_mem_tmp.indexName);

        else if($expr_mem_tmp.func){
            $flag = true;
            if($expr_other.name.equals("head")){
                $t = $expr_mem_tmp.headType;
                pc = addToStack($top, $expr_mem_tmp.name, pc);
            }
            else if($expr_other.name.equals("tail")){
                $t = $expr_mem_tmp.tailType;
                pc = addToStack($top, $expr_mem_tmp.name, pc);
            }
            else if($expr_other.name.equals("size")){
                $t = $expr_other.t;
                pc = addToStack($expr_mem_tmp.args.get(0).getR().t.size(), pc);
            }
            else if($expr_other.name.equals("read"))
                for(int i=0;i<$expr_mem_tmp.num;i++){
                    $t.addType("", new Type("char"));
                    PB[pc] = "li \$v0, 12";
                    PB[pc+1] = "syscall";
                    PB[pc+2] = "sw \$v0, 0(\$sp)";
                    PB[pc+3] = "addiu \$sp, \$sp, -4";
                    pc += 4;
                }
            else if($expr_other.name.equals("write")){
                if(isAccFull){
                    PB[pc] = "sw \$a0, 0(\$sp)";
                    PB[pc+1] = "addiu \$sp, \$sp, -4";
                    isAccFull = false;
                    pc += 2;
                }
                if($top.getAdr($expr_mem_tmp.name)!=-1){
                    PB[pc] = "lw \$a0, -"+$top.getAdr($expr_mem_tmp.name)+"(\$fp)";
                    PB[pc+1] = "li \$v0, 11";
                    PB[pc+2] = "syscall";
                    pc += 3;
                    for(int i=1; i<$expr_mem_tmp.args.get(0).getR().t.size(); i++){
                        PB[pc] = "lw \$a0, -"+($top.getAdr($expr_mem_tmp.name)+(i*4))+"(\$fp)";
                        PB[pc+1] = "li \$v0, 11";
                        PB[pc+2] = "syscall";
                        pc += 3;
                    }
                }else{
                    int i;
                    for(i=0; i<$expr_mem_tmp.args.get(0).getR().t.size(); i++){
                        PB[pc] = "lw \$a0, "+(($expr_mem_tmp.args.get(0).getR().t.size()-i)*4)+"(\$sp)";
                        PB[pc+1] = "li \$v0, 11";
                        PB[pc+2] = "syscall";
                        pc += 3;
                    }
                    PB[pc] = "addiu \$sp, \$sp, "+(i*4);
                    pc++;
                }
            }
        }
    }
    ;
expr_mem_tmp [Env top] returns [String name, String indexName, boolean dot, Type headType, Type tailType, int num,  
                                boolean func, boolean sharp, ArrayList<Pair<String, Type>> args]
    :{
        $dot=false;
        $headType = new Type("nil");
        $tailType = new Type("nil");
        $func=false;
        $sharp=false;
        $args = new ArrayList<Pair<String, Type>>();
        $num = 0;
    }
    ('#' expr_other[$top] {$num=$expr_other.num;$sharp=true;}
    |'.' ID {$indexName = $ID.text;$dot=true;}
    |'(' (e1=expr[$top]
    {
        $name = $e1.text;
        if($e1.t!=null && $e1.t.name.equals("list") && $e1.t.t.size()!=0){
            $headType = $e1.t;
            $tailType = $e1.t;
        }
        $num = $e1.num;
        Pair newPair = new Pair("", $e1.t);
        $args.add(newPair);
    } (',' e2=expr[$top]
    {
        $tailType = $e2.t;
        Pair newPair2 = new Pair("", $e2.t);
        $args.add(newPair2);
    })*)? ')' ) {$func=true;} expr_mem_tmp[$top] 
    |
    ;
expr_other [Env top] returns [ String name, Type t, boolean isList, boolean undef, int num]
    locals [Type temp]
    : 
    {
        $isList = false;
        $name="";
        $undef = false;
        $num = 0;
    }
    (CONST_NUM 
    {
        $num = Integer.parseInt($CONST_NUM.text);
        $t = new Type("int");
        pc = addToStack($num, pc);
    }
    | CONST_CHAR 
    {
        $t = new Type("char");
        pc = addToStack($CONST_CHAR.text.charAt(1), pc);
    }
    | CONST_STR 
    {
        $t = new Type("list");
        $temp = new Type("char");
        for(int i=0;i<$CONST_STR.text.length()-2;i++){
            $t.addType("", $temp);
            pc = addToStack($CONST_STR.text.charAt(i+1), pc);
        }
        $isList = true;
    }   
    | CONST_FUNC 
    {
        $name = $CONST_FUNC.text;
        switch($CONST_FUNC.text){
            case("head"):
                $t = new Type("head");
            break;
            case("tail"):
                $t = new Type("tail");
            break;
            case("size"):
                $t = new Type("int");
            break;
            case("read"):
                $t = new Type("list");
                $isList = true;
            break;
            case("write"):
                $t = new Type("void");
            break;
        }
        
    }
    | ID 
    {
        $name = $ID.text; 
        if(prog.getType($top, $name) == null){
            $undef = true;
            $t = new Type("nil");
        }
        else{
            $t = prog.getType($top, $name);
            // pc = addToStack($top, $name, pc);
            if($t.name.equals("list"))
        	    $isList = true;
        }
    }
    | {$t = new Type("list");}a='[' (e1=expr[$top] {$t.addType("", $e1.t);} (',' e2=expr[$top]{$t.addType("", $e2.t);})*)?']' 
        {$isList = true;}
    | a='nil' {$t = new Type("nil");} 
    | a='(' expr[$top] ')' {$t = $expr.t;})
    ;
while_stm [Type ret, Env top] locals [String label1, String label2, int whileJmp]
    : {breakAdr.add(-1);}'while' 
    {
        $label1 = labelGenerator(3); 
        PB[pc] = $label1+":";
        pc++;
    }'(' expr[$top]
    {
        $whileJmp = pc;
        pc++;
        isBreak = false;
    } ')'statement[$ret, $top]
    {
        $label2 = labelGenerator(3);
        PB[$whileJmp] = "blez \$a0, " + $label2;
        PB[pc] = "j " + $label1;
        PB[pc+1] = $label2+ ":";
        pc += 2;

        while(breakAdr.get(breakAdr.size()-1) != -1){
            PB[breakAdr.get(breakAdr.size()-1)] = "j " + $label2;
            breakAdr.remove(breakAdr.get(breakAdr.size()-1));
        }
        breakAdr.remove(breakAdr.get(breakAdr.size()-1));
    }
    ;
foreach_stm [Type ret, Env top] locals [int size, int i]
    : 'foreach' '(' type ID {$top.put($ID.text, $type.t);} 'in' expr[$top] {$i = 0; $size = $expr.t.size/4;}')' 
    statement[$ret, $top]
    ;
return_stm [Type ret, Env top]
    : 'return' (expr[$top])?
    ;
break_stm
    : 'break' 
    {
        isBreak = true;
        breakAdr.add(pc);
    }
    ;

CONST_NUM:
    [0-9]+ ('.' [0-9]+)?
    ;
CONST_CHAR:
    '\'' . '\''
    ;
CONST_STR:
    '"' ~('\r' | '\n' | '"')* '"'
    ;
CONST_FUNC:
    'tail' | 'head' | 'size' | 'read' | 'write'
    ;
ID:
    [a-zA-Z_][a-zA-Z0-9_]*
    ;
SLC:
    '//'(~[\r\n])* -> skip 
    ;
MLC:
    '/*'((~[*])|'*'(~[/])+)*('*')* '*/' -> skip  
    ;
WS:
    [ \r\t\n]+ -> skip
    ;
