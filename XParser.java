// Generated from X.g4 by ANTLR 4.5

    import java.util.Vector;
    import java.util.Random;
    import java.util.ArrayList;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class XParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, CONST_NUM=42, CONST_CHAR=43, CONST_STR=44, 
		CONST_FUNC=45, ID=46, SLC=47, MLC=48, WS=49;
	public static final int
		RULE_program = 0, RULE_fordecl = 1, RULE_type_fordecl = 2, RULE_func_fordecl = 3, 
		RULE_funcdef = 4, RULE_typedef = 5, RULE_vardef = 6, RULE_type = 7, RULE_block = 8, 
		RULE_statement = 9, RULE_other_stm = 10, RULE_if_stm = 11, RULE_if_stm_else = 12, 
		RULE_expr = 13, RULE_expr_assign = 14, RULE_expr_or = 15, RULE_expr_or_tmp = 16, 
		RULE_expr_and = 17, RULE_expr_and_tmp = 18, RULE_expr_eq = 19, RULE_expr_eq_tmp = 20, 
		RULE_expr_cmp = 21, RULE_expr_cmp_tmp = 22, RULE_expr_add = 23, RULE_expr_add_tmp = 24, 
		RULE_expr_mult = 25, RULE_expr_mult_tmp = 26, RULE_expr_un = 27, RULE_expr_mem = 28, 
		RULE_expr_mem_tmp = 29, RULE_expr_other = 30, RULE_while_stm = 31, RULE_foreach_stm = 32, 
		RULE_return_stm = 33, RULE_break_stm = 34;
	public static final String[] ruleNames = {
		"program", "fordecl", "type_fordecl", "func_fordecl", "funcdef", "typedef", 
		"vardef", "type", "block", "statement", "other_stm", "if_stm", "if_stm_else", 
		"expr", "expr_assign", "expr_or", "expr_or_tmp", "expr_and", "expr_and_tmp", 
		"expr_eq", "expr_eq_tmp", "expr_cmp", "expr_cmp_tmp", "expr_add", "expr_add_tmp", 
		"expr_mult", "expr_mult_tmp", "expr_un", "expr_mem", "expr_mem_tmp", "expr_other", 
		"while_stm", "foreach_stm", "return_stm", "break_stm"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "';'", "'typedef'", "'('", "':'", "','", "')'", "'$'", "'as'", "'='", 
		"'['", "']'", "'list'", "'of'", "'<'", "'*'", "'->'", "'>'", "'string'", 
		"'char'", "'int'", "'void'", "'{'", "'}'", "'if'", "'else'", "'||'", "'&&'", 
		"'=='", "'!='", "'+'", "'-'", "'/'", "'!'", "'#'", "'.'", "'nil'", "'while'", 
		"'foreach'", "'in'", "'return'", "'break'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, "CONST_NUM", "CONST_CHAR", "CONST_STR", 
		"CONST_FUNC", "ID", "SLC", "MLC", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "X.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	    X prog= new X();
	    void print(String str){
	        System.out.println(str);
	    }
	    String[] PB = new String[1000];
	    int pc = 0;
	    boolean isAccFull=false;
	    int addToStack(Env cur, String varName, int pc){
	        for(int i=0; i<cur.getSize(varName)/4; i++){
	            if(isAccFull){
	                PB[pc] = "sw $a0, 0($sp)";
	                PB[pc+1] = "addiu $sp, $sp, -4";
	                pc += 2;
	            }
	            else
	                isAccFull=true;
	            PB[pc] = "lw $a0, -"+((cur.getAdr(varName)+i)*4)+"($fp)";
	            pc++;
	        }
	        return pc;
	    }
	    int addToStack(int val, int pc){
	        if(isAccFull){
	            PB[pc] = "sw $a0, 0($sp)";
	            PB[pc+1] = "addiu $sp, $sp, -4";
	            pc += 2;
	        }
	        else
	            isAccFull=true;
	        PB[pc] = "li $a0, "+val;
	        pc++;
	        return pc;
	    }
	    int addToStack(char c, int pc){
	        if(isAccFull){
	            PB[pc] = "sw $a0, 0($sp)";
	            PB[pc+1] = "addiu $sp, $sp, -4";
	            pc += 2;
	        }
	        else
	            isAccFull=true;
	        PB[pc] = "li $a0, \'"+c+"\'";
	        pc++;    
	        return pc;
	    }
	    String operationCommand(String op){
	        if("+".equals(op))
	            return "add $a0, $a0, $t1";
	        if("-".equals(op))
	            return "sub $a0, $a0, $t1";
	        if("*".equals(op))
	            return "mul $a0, $a0, $t1";
	        if("/".equals(op))
	            return "div $a0, $t1, $a0";//note it
	        return "";
	    }
	    public static String labelGenerator(int len){
	        final String tmp = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	        Random rnd = new Random();
	        StringBuilder sb = new StringBuilder( len );
	        for( int i = 0; i < len; i++ ) 
	           sb.append( tmp.charAt( rnd.nextInt(tmp.length()) ) );
	        return sb.toString();
	    }
	    ArrayList<Integer> ifJmpadr = new ArrayList<Integer>();
	    ArrayList<Integer> elseJmpadr = new ArrayList<Integer>();
	    ArrayList<Integer> breakAdr = new ArrayList<Integer>();
	    boolean isBreak = false;

	public XParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public Token a;
		public List<FordeclContext> fordecl() {
			return getRuleContexts(FordeclContext.class);
		}
		public FordeclContext fordecl(int i) {
			return getRuleContext(FordeclContext.class,i);
		}
		public List<FuncdefContext> funcdef() {
			return getRuleContexts(FuncdefContext.class);
		}
		public FuncdefContext funcdef(int i) {
			return getRuleContext(FuncdefContext.class,i);
		}
		public List<TypedefContext> typedef() {
			return getRuleContexts(TypedefContext.class);
		}
		public TypedefContext typedef(int i) {
			return getRuleContext(TypedefContext.class,i);
		}
		public List<VardefContext> vardef() {
			return getRuleContexts(VardefContext.class);
		}
		public VardefContext vardef(int i) {
			return getRuleContext(VardefContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{

			        Env top = prog.addEnv(null);
			        PB[pc] = "main:";
			        PB[pc+1] = "move $fp $sp";
			        pc += 2;
			    
			setState(81); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(81);
				switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
				case 1:
					{
					setState(71);
					fordecl();
					setState(72);
					((ProgramContext)_localctx).a = match(T__0);
					}
					break;
				case 2:
					{
					setState(74);
					funcdef(top);
					}
					break;
				case 3:
					{
					setState(75);
					typedef(top);
					setState(76);
					match(T__0);
					}
					break;
				case 4:
					{
					setState(78);
					vardef(true, top);
					setState(79);
					match(T__0);
					}
					break;
				}
				}
				setState(83); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__9) | (1L << T__11) | (1L << T__13) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << ID))) != 0) );

			        // prog.printEnvs();
			        PB[pc] = "li $v0, 10";
			        PB[pc+1] = "syscall";
			        pc += 2;
			        for(String s : PB){
			            if(s==null)
			                break;
			            print(s);
			        }
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FordeclContext extends ParserRuleContext {
		public Type retType;
		public String name;
		public ArrayList<Pair<String, Type>> argus;
		public Func_fordeclContext ff;
		public Func_fordeclContext func_fordecl() {
			return getRuleContext(Func_fordeclContext.class,0);
		}
		public Type_fordeclContext type_fordecl() {
			return getRuleContext(Type_fordeclContext.class,0);
		}
		public FordeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fordecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterFordecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitFordecl(this);
		}
	}

	public final FordeclContext fordecl() throws RecognitionException {
		FordeclContext _localctx = new FordeclContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_fordecl);
		try {
			setState(91);
			switch (_input.LA(1)) {
			case T__9:
			case T__11:
			case T__13:
			case T__17:
			case T__18:
			case T__19:
			case T__20:
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(87);
				((FordeclContext)_localctx).ff = func_fordecl();
				((FordeclContext)_localctx).retType =  ((FordeclContext)_localctx).ff.retType; ((FordeclContext)_localctx).name =  ((FordeclContext)_localctx).ff.name; ((FordeclContext)_localctx).argus =  ((FordeclContext)_localctx).ff.argus;
				}
				break;
			case T__1:
				enterOuterAlt(_localctx, 2);
				{
				setState(90);
				type_fordecl();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_fordeclContext extends ParserRuleContext {
		public String name;
		public Token ID;
		public TerminalNode ID() { return getToken(XParser.ID, 0); }
		public Type_fordeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_fordecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterType_fordecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitType_fordecl(this);
		}
	}

	public final Type_fordeclContext type_fordecl() throws RecognitionException {
		Type_fordeclContext _localctx = new Type_fordeclContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_type_fordecl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(93);
			match(T__1);
			setState(94);
			((Type_fordeclContext)_localctx).ID = match(ID);

			        ((Type_fordeclContext)_localctx).name =  (((Type_fordeclContext)_localctx).ID!=null?((Type_fordeclContext)_localctx).ID.getText():null);
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_fordeclContext extends ParserRuleContext {
		public Type retType;
		public String name;
		public ArrayList<Pair<String, Type>> argus;
		public TypeContext t1;
		public Token func_name;
		public Token n2;
		public TypeContext t2;
		public Token n3;
		public TypeContext t3;
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TerminalNode> ID() { return getTokens(XParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(XParser.ID, i);
		}
		public Func_fordeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_fordecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterFunc_fordecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitFunc_fordecl(this);
		}
	}

	public final Func_fordeclContext func_fordecl() throws RecognitionException {
		Func_fordeclContext _localctx = new Func_fordeclContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_func_fordecl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97);
			((Func_fordeclContext)_localctx).t1 = type();
			setState(98);
			((Func_fordeclContext)_localctx).func_name = match(ID);
			setState(99);
			match(T__2);

			        ((Func_fordeclContext)_localctx).retType =  ((Func_fordeclContext)_localctx).t1.t;
			        ((Func_fordeclContext)_localctx).name =  (((Func_fordeclContext)_localctx).func_name!=null?((Func_fordeclContext)_localctx).func_name.getText():null);
			    
			setState(120);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__9) | (1L << T__11) | (1L << T__13) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << ID))) != 0)) {
				{
				setState(103);
				switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
				case 1:
					{
					setState(101);
					((Func_fordeclContext)_localctx).n2 = match(ID);
					setState(102);
					match(T__3);
					}
					break;
				}
				setState(105);
				((Func_fordeclContext)_localctx).t2 = type();

				        ((Func_fordeclContext)_localctx).argus =  new ArrayList<Pair<String, Type>>();
				        Pair<String, Type> newPair = new Pair((((Func_fordeclContext)_localctx).n2!=null?((Func_fordeclContext)_localctx).n2.getText():null), ((Func_fordeclContext)_localctx).t2.t); 
				        _localctx.argus.add(newPair);
				    
				setState(117);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__4) {
					{
					{
					setState(107);
					match(T__4);
					setState(110);
					switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
					case 1:
						{
						setState(108);
						((Func_fordeclContext)_localctx).n3 = match(ID);
						setState(109);
						match(T__3);
						}
						break;
					}
					setState(112);
					((Func_fordeclContext)_localctx).t3 = type();

					        Pair<String, Type> newPair2 = new Pair((((Func_fordeclContext)_localctx).n3!=null?((Func_fordeclContext)_localctx).n3.getText():null), ((Func_fordeclContext)_localctx).t3.t); 
					        _localctx.argus.add(newPair2);
					    
					}
					}
					setState(119);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(122);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncdefContext extends ParserRuleContext {
		public Env top;
		public ArrayList<Pair<String, Type>> args;
		public TypeContext t1;
		public Token func_name;
		public Token n2;
		public TypeContext t2;
		public Token n3;
		public TypeContext t3;
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TerminalNode> ID() { return getTokens(XParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(XParser.ID, i);
		}
		public FuncdefContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public FuncdefContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_funcdef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterFuncdef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitFuncdef(this);
		}
	}

	public final FuncdefContext funcdef(Env top) throws RecognitionException {
		FuncdefContext _localctx = new FuncdefContext(_ctx, getState(), top);
		enterRule(_localctx, 8, RULE_funcdef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(124);
			((FuncdefContext)_localctx).t1 = type();
			setState(125);
			((FuncdefContext)_localctx).func_name = match(ID);
			setState(126);
			match(T__2);
			setState(148);
			_la = _input.LA(1);
			if (_la==T__6 || _la==ID) {
				{
				setState(128);
				_la = _input.LA(1);
				if (_la==T__6) {
					{
					setState(127);
					match(T__6);
					}
				}

				setState(130);
				((FuncdefContext)_localctx).n2 = match(ID);
				setState(131);
				match(T__3);
				setState(132);
				((FuncdefContext)_localctx).t2 = type();

				        ((FuncdefContext)_localctx).args =  new ArrayList<Pair<String, Type>>();
				        Pair<String, Type> newPair = new Pair((((FuncdefContext)_localctx).n2!=null?((FuncdefContext)_localctx).n2.getText():null), ((FuncdefContext)_localctx).t2.t); 
				        _localctx.args.add(newPair);
				        _localctx.top.put((((FuncdefContext)_localctx).n2!=null?((FuncdefContext)_localctx).n2.getText():null), ((FuncdefContext)_localctx).t2.t);
				    
				setState(145);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__4) {
					{
					{
					setState(134);
					match(T__4);
					setState(136);
					_la = _input.LA(1);
					if (_la==T__6) {
						{
						setState(135);
						match(T__6);
						}
					}

					setState(138);
					((FuncdefContext)_localctx).n3 = match(ID);
					setState(139);
					match(T__3);
					setState(140);
					((FuncdefContext)_localctx).t3 = type();

					        Pair<String, Type> newPair2 = new Pair((((FuncdefContext)_localctx).n3!=null?((FuncdefContext)_localctx).n3.getText():null), ((FuncdefContext)_localctx).t3.t); 
					        _localctx.args.add(newPair2);
					        _localctx.top.put((((FuncdefContext)_localctx).n3!=null?((FuncdefContext)_localctx).n3.getText():null), ((FuncdefContext)_localctx).t3.t);
					    
					}
					}
					setState(147);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(150);
			match(T__5);
			setState(151);
			block(((FuncdefContext)_localctx).t1.t, _localctx.top);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypedefContext extends ParserRuleContext {
		public Env top;
		public Type_fordeclContext type_fordecl;
		public Token a;
		public TypeContext type;
		public Type_fordeclContext type_fordecl() {
			return getRuleContext(Type_fordeclContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TypedefContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public TypedefContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_typedef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterTypedef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitTypedef(this);
		}
	}

	public final TypedefContext typedef(Env top) throws RecognitionException {
		TypedefContext _localctx = new TypedefContext(_ctx, getState(), top);
		enterRule(_localctx, 10, RULE_typedef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(153);
			((TypedefContext)_localctx).type_fordecl = type_fordecl();
			setState(154);
			((TypedefContext)_localctx).a = match(T__7);
			setState(155);
			((TypedefContext)_localctx).type = type();

			        _localctx.top.put(((TypedefContext)_localctx).type_fordecl.name, ((TypedefContext)_localctx).type.t);
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VardefContext extends ParserRuleContext {
		public boolean isGlobal;
		public Env top;
		public TypeContext type;
		public Token n1;
		public ExprContext e1;
		public Token n2;
		public Token a;
		public ExprContext e2;
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public List<TerminalNode> ID() { return getTokens(XParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(XParser.ID, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public VardefContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public VardefContext(ParserRuleContext parent, int invokingState, boolean isGlobal, Env top) {
			super(parent, invokingState);
			this.isGlobal = isGlobal;
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_vardef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterVardef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitVardef(this);
		}
	}

	public final VardefContext vardef(boolean isGlobal,Env top) throws RecognitionException {
		VardefContext _localctx = new VardefContext(_ctx, getState(), isGlobal, top);
		enterRule(_localctx, 12, RULE_vardef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(158);
			((VardefContext)_localctx).type = type();
			setState(159);
			((VardefContext)_localctx).n1 = match(ID);
			_localctx.top.put((((VardefContext)_localctx).n1!=null?((VardefContext)_localctx).n1.getText():null), ((VardefContext)_localctx).type.t);
			setState(163);
			_la = _input.LA(1);
			if (_la==T__8) {
				{
				setState(161);
				match(T__8);
				setState(162);
				((VardefContext)_localctx).e1 = expr(_localctx.top);
				}
			}

			setState(174);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__4) {
				{
				{
				setState(165);
				match(T__4);
				setState(166);
				((VardefContext)_localctx).n2 = match(ID);
				_localctx.top.put((((VardefContext)_localctx).n2!=null?((VardefContext)_localctx).n2.getText():null), ((VardefContext)_localctx).type.t);
				setState(170);
				_la = _input.LA(1);
				if (_la==T__8) {
					{
					setState(168);
					((VardefContext)_localctx).a = match(T__8);
					setState(169);
					((VardefContext)_localctx).e2 = expr(_localctx.top);
					}
				}

				}
				}
				setState(176);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public Type t;
		public boolean flag;
		public String res;
		public Type temp;
		public ArrayList<Type> types;
		public Token ID;
		public Token a;
		public Token n1;
		public TypeContext t1;
		public Token b;
		public Token n2;
		public TypeContext t2;
		public Token CONST_NUM;
		public TypeContext t3;
		public List<TerminalNode> ID() { return getTokens(XParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(XParser.ID, i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public TerminalNode CONST_NUM() { return getToken(XParser.CONST_NUM, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_type);
		int _la;
		try {
			setState(241);
			switch (_input.LA(1)) {
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(177);
				((TypeContext)_localctx).ID = match(ID);
				((TypeContext)_localctx).t =  new Type((((TypeContext)_localctx).ID!=null?((TypeContext)_localctx).ID.getText():null));
				}
				break;
			case T__9:
				enterOuterAlt(_localctx, 2);
				{
				((TypeContext)_localctx).res =  "";((TypeContext)_localctx).t =  new Type("list");
				setState(180);
				((TypeContext)_localctx).a = match(T__9);
				setState(184);
				switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
				case 1:
					{
					setState(181);
					((TypeContext)_localctx).n1 = match(ID);
					((TypeContext)_localctx).res =  (((TypeContext)_localctx).n1!=null?((TypeContext)_localctx).n1.getText():null);
					setState(183);
					match(T__3);
					}
					break;
				}
				setState(186);
				((TypeContext)_localctx).t1 = type();

				        _localctx.t.addType(_localctx.res, ((TypeContext)_localctx).t1.t);
				        ((TypeContext)_localctx).res =  "";
				    
				setState(199);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__4) {
					{
					{
					setState(188);
					((TypeContext)_localctx).b = match(T__4);
					setState(192);
					switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
					case 1:
						{
						setState(189);
						((TypeContext)_localctx).n2 = match(ID);
						((TypeContext)_localctx).res =  (((TypeContext)_localctx).n2!=null?((TypeContext)_localctx).n2.getText():null);
						setState(191);
						match(T__3);
						}
						break;
					}
					setState(194);
					((TypeContext)_localctx).t2 = type();

					        _localctx.t.addType(_localctx.res, ((TypeContext)_localctx).t2.t);
					    
					}
					}
					setState(201);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(202);
				match(T__10);
				}
				break;
			case T__11:
				enterOuterAlt(_localctx, 3);
				{
				setState(204);
				match(T__11);
				setState(205);
				match(T__2);
				setState(206);
				((TypeContext)_localctx).CONST_NUM = match(CONST_NUM);
				setState(207);
				match(T__5);
				setState(208);
				match(T__12);
				setState(209);
				((TypeContext)_localctx).t1 = type();

				        ((TypeContext)_localctx).t =  new Type("list");
				        ((TypeContext)_localctx).temp =  ((TypeContext)_localctx).t1.t;
				        for(int i=0;i<Integer.parseInt((((TypeContext)_localctx).CONST_NUM!=null?((TypeContext)_localctx).CONST_NUM.getText():null));i++)
				            _localctx.t.addType("", _localctx.temp);
				    
				}
				break;
			case T__13:
				enterOuterAlt(_localctx, 4);
				{
				setState(212);
				((TypeContext)_localctx).a = match(T__13);
				{
				setState(213);
				((TypeContext)_localctx).t1 = type();

				    	((TypeContext)_localctx).types =  new ArrayList<>();
				        _localctx.types.add(new Type(((TypeContext)_localctx).t1.t));
				    
				setState(221);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__14) {
					{
					{
					setState(215);
					match(T__14);
					setState(216);
					((TypeContext)_localctx).t2 = type();
					_localctx.types.add(new Type(((TypeContext)_localctx).t2.t));
					}
					}
					setState(223);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}

				            ((TypeContext)_localctx).temp =  new Type("->");
				        
				setState(225);
				match(T__15);
				setState(226);
				((TypeContext)_localctx).t3 = type();

				            ((TypeContext)_localctx).t =  new Type(((TypeContext)_localctx).t3.t);
				            for(int i=0;i<_localctx.types.size();i++)
				                _localctx.t.addType("", _localctx.types.get(i));
				        
				setState(228);
				match(T__16);
				}
				break;
			case T__17:
				enterOuterAlt(_localctx, 5);
				{
				setState(230);
				match(T__17);
				setState(231);
				match(T__2);
				setState(232);
				((TypeContext)_localctx).CONST_NUM = match(CONST_NUM);
				setState(233);
				match(T__5);
				   
				        ((TypeContext)_localctx).t =  new Type("list");
				        ((TypeContext)_localctx).temp =  new Type("char");
				        for(int i=0;i<Integer.parseInt((((TypeContext)_localctx).CONST_NUM!=null?((TypeContext)_localctx).CONST_NUM.getText():null));i++)
				            _localctx.t.addType("", _localctx.temp);
				    
				}
				break;
			case T__18:
				enterOuterAlt(_localctx, 6);
				{
				setState(235);
				match(T__18);
				((TypeContext)_localctx).t =  new Type("char");
				}
				break;
			case T__19:
				enterOuterAlt(_localctx, 7);
				{
				setState(237);
				match(T__19);
				((TypeContext)_localctx).t =  new Type("int");
				}
				break;
			case T__20:
				enterOuterAlt(_localctx, 8);
				{
				setState(239);
				match(T__20);
				((TypeContext)_localctx).t =  new Type("void");
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public Type ret;
		public Env prev;
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public BlockContext(ParserRuleContext parent, int invokingState, Type ret, Env prev) {
			super(parent, invokingState);
			this.ret = ret;
			this.prev = prev;
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitBlock(this);
		}
	}

	public final BlockContext block(Type ret,Env prev) throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState(), ret, prev);
		enterRule(_localctx, 16, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(243);
			match(T__21);
			Env top = prog.addEnv(_localctx.prev);
			setState(248);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__9) | (1L << T__11) | (1L << T__13) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__23) | (1L << T__30) | (1L << T__32) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__39) | (1L << T__40) | (1L << CONST_NUM) | (1L << CONST_CHAR) | (1L << CONST_STR) | (1L << CONST_FUNC) | (1L << ID))) != 0)) {
				{
				{
				setState(245);
				statement(_localctx.ret, top);
				}
				}
				setState(250);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}

			        PB[pc] = "addiu $sp, $sp, "+top.offset;
			        isBreak = false;
			    
			setState(252);
			match(T__22);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public Type ret;
		public Env top;
		public If_stmContext if_stm() {
			return getRuleContext(If_stmContext.class,0);
		}
		public Other_stmContext other_stm() {
			return getRuleContext(Other_stmContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public StatementContext(ParserRuleContext parent, int invokingState, Type ret, Env top) {
			super(parent, invokingState);
			this.ret = ret;
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement(Type ret,Env top) throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState(), ret, top);
		enterRule(_localctx, 18, RULE_statement);
		try {
			setState(256);
			switch (_input.LA(1)) {
			case T__23:
				enterOuterAlt(_localctx, 1);
				{
				setState(254);
				if_stm(_localctx.ret,_localctx.top);
				}
				break;
			case T__2:
			case T__9:
			case T__11:
			case T__13:
			case T__17:
			case T__18:
			case T__19:
			case T__20:
			case T__21:
			case T__30:
			case T__32:
			case T__35:
			case T__36:
			case T__37:
			case T__39:
			case T__40:
			case CONST_NUM:
			case CONST_CHAR:
			case CONST_STR:
			case CONST_FUNC:
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(255);
				other_stm(_localctx.ret, _localctx.top);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Other_stmContext extends ParserRuleContext {
		public Type ret;
		public Env top;
		public VardefContext vardef() {
			return getRuleContext(VardefContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Return_stmContext return_stm() {
			return getRuleContext(Return_stmContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public While_stmContext while_stm() {
			return getRuleContext(While_stmContext.class,0);
		}
		public Foreach_stmContext foreach_stm() {
			return getRuleContext(Foreach_stmContext.class,0);
		}
		public Break_stmContext break_stm() {
			return getRuleContext(Break_stmContext.class,0);
		}
		public Other_stmContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Other_stmContext(ParserRuleContext parent, int invokingState, Type ret, Env top) {
			super(parent, invokingState);
			this.ret = ret;
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_other_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterOther_stm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitOther_stm(this);
		}
	}

	public final Other_stmContext other_stm(Type ret,Env top) throws RecognitionException {
		Other_stmContext _localctx = new Other_stmContext(_ctx, getState(), ret, top);
		enterRule(_localctx, 20, RULE_other_stm);
		try {
			setState(273);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(258);
				vardef(false, _localctx.top);
				setState(259);
				match(T__0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(261);
				expr(_localctx.top);
				setState(262);
				match(T__0);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(264);
				return_stm(_localctx.ret, _localctx.top);
				setState(265);
				match(T__0);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(267);
				block(_localctx.ret, _localctx.top);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(268);
				while_stm(_localctx.ret, _localctx.top);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(269);
				foreach_stm(_localctx.ret,prog.addEnv(_localctx.top));
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(270);
				break_stm();
				setState(271);
				match(T__0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_stmContext extends ParserRuleContext {
		public Type ret;
		public Env top;
		public String label1;
		public String label2;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public If_stm_elseContext if_stm_else() {
			return getRuleContext(If_stm_elseContext.class,0);
		}
		public If_stmContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public If_stmContext(ParserRuleContext parent, int invokingState, Type ret, Env top) {
			super(parent, invokingState);
			this.ret = ret;
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_if_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterIf_stm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitIf_stm(this);
		}
	}

	public final If_stmContext if_stm(Type ret,Env top) throws RecognitionException {
		If_stmContext _localctx = new If_stmContext(_ctx, getState(), ret, top);
		enterRule(_localctx, 22, RULE_if_stm);
		try {
			setState(294);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(275);
				match(T__23);
				setState(276);
				match(T__2);
				setState(277);
				expr(_localctx.top);
				setState(278);
				match(T__5);

				        isBreak = false;
				        ifJmpadr.add(pc);
				        pc++;
				    
				setState(280);
				statement(_localctx.ret, _localctx.top);

				        ((If_stmContext)_localctx).label1 =  labelGenerator(3);
				        PB[ifJmpadr.get(ifJmpadr.size()-1)] = "blez $a0, " + _localctx.label1;
				        ifJmpadr.remove(ifJmpadr.get(ifJmpadr.size()-1));
				        PB[pc] = _localctx.label1+ ":";
				        pc++;
				    
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(283);
				match(T__23);
				setState(284);
				match(T__2);
				setState(285);
				expr(_localctx.top);
				setState(286);
				match(T__5);

				        isBreak = false;
				        ifJmpadr.add(pc);
				        pc++;
				    
				setState(288);
				if_stm_else(_localctx.ret,_localctx.top);
				setState(289);
				match(T__24);

				        ((If_stmContext)_localctx).label1 =  labelGenerator(3); 
				        PB[ifJmpadr.get(ifJmpadr.size()-1)] = "blez $a0, " + _localctx.label1;
				        ifJmpadr.remove(ifJmpadr.get(ifJmpadr.size()-1));
				        elseJmpadr.add(pc) ; 
				        PB[pc+1] = _localctx.label1+ ":";
				        pc += 2;
				        isBreak = false;
				    
				setState(291);
				statement(_localctx.ret, _localctx.top);

				        ((If_stmContext)_localctx).label2 =  labelGenerator(3);
				        PB[pc] = _localctx.label2+ ":";
				        PB[elseJmpadr.get(elseJmpadr.size()-1)] = "j " + _localctx.label2;
				        elseJmpadr.remove(elseJmpadr.get(elseJmpadr.size()-1));
				        pc++;
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_stm_elseContext extends ParserRuleContext {
		public Type ret;
		public Env top;
		public String label1;
		public String label2;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<If_stm_elseContext> if_stm_else() {
			return getRuleContexts(If_stm_elseContext.class);
		}
		public If_stm_elseContext if_stm_else(int i) {
			return getRuleContext(If_stm_elseContext.class,i);
		}
		public Other_stmContext other_stm() {
			return getRuleContext(Other_stmContext.class,0);
		}
		public If_stm_elseContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public If_stm_elseContext(ParserRuleContext parent, int invokingState, Type ret, Env top) {
			super(parent, invokingState);
			this.ret = ret;
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_if_stm_else; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterIf_stm_else(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitIf_stm_else(this);
		}
	}

	public final If_stm_elseContext if_stm_else(Type ret,Env top) throws RecognitionException {
		If_stm_elseContext _localctx = new If_stm_elseContext(_ctx, getState(), ret, top);
		enterRule(_localctx, 24, RULE_if_stm_else);
		try {
			setState(308);
			switch (_input.LA(1)) {
			case T__23:
				enterOuterAlt(_localctx, 1);
				{
				setState(296);
				match(T__23);
				setState(297);
				match(T__2);
				setState(298);
				expr(_localctx.top);
				setState(299);
				match(T__5);

				        isBreak = false;
				        ifJmpadr.add(pc);
				        pc++;
				    
				setState(301);
				if_stm_else(_localctx.ret,prog.addEnv(_localctx.top));
				setState(302);
				match(T__24);

				        ((If_stm_elseContext)_localctx).label1 =  labelGenerator(3); 
				        PB[ifJmpadr.get(ifJmpadr.size()-1)] = "blez $a0, " + _localctx.label1;
				        ifJmpadr.remove(ifJmpadr.get(ifJmpadr.size()-1));
				        elseJmpadr.add(pc) ; 
				        PB[pc+1] = _localctx.label1+ ":";
				        pc += 2;
				        isBreak = false;
				    
				setState(304);
				if_stm_else(_localctx.ret,prog.addEnv(_localctx.top));

				        ((If_stm_elseContext)_localctx).label2 =  labelGenerator(3);
				        PB[pc] = _localctx.label2+ ":";
				        PB[elseJmpadr.get(elseJmpadr.size()-1)] = "j " + _localctx.label2;
				        elseJmpadr.remove(elseJmpadr.get(elseJmpadr.size()-1));
				        pc++;
				    
				}
				break;
			case T__2:
			case T__9:
			case T__11:
			case T__13:
			case T__17:
			case T__18:
			case T__19:
			case T__20:
			case T__21:
			case T__30:
			case T__32:
			case T__35:
			case T__36:
			case T__37:
			case T__39:
			case T__40:
			case CONST_NUM:
			case CONST_CHAR:
			case CONST_STR:
			case CONST_FUNC:
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(307);
				other_stm(_localctx.ret, _localctx.top);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public Env top;
		public String name;
		public Type t;
		public int num;
		public Expr_assignContext expr_assign;
		public Expr_assignContext expr_assign() {
			return getRuleContext(Expr_assignContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ExprContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr(this);
		}
	}

	public final ExprContext expr(Env top) throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState(), top);
		enterRule(_localctx, 26, RULE_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(310);
			((ExprContext)_localctx).expr_assign = expr_assign(_localctx.top);
			        
			        ((ExprContext)_localctx).name =  ((ExprContext)_localctx).expr_assign.name;
			        ((ExprContext)_localctx).t =  ((ExprContext)_localctx).expr_assign.t;
			        ((ExprContext)_localctx).num =  ((ExprContext)_localctx).expr_assign.num;
			        // System.out.println(_localctx.num);
			        // print(_localctx.name);
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_assignContext extends ParserRuleContext {
		public Env top;
		public String name;
		public Type t;
		public int num;
		public Expr_orContext expr_or;
		public Token a;
		public Expr_assignContext ea;
		public Expr_assignContext expr_assign;
		public Expr_orContext expr_or() {
			return getRuleContext(Expr_orContext.class,0);
		}
		public Expr_assignContext expr_assign() {
			return getRuleContext(Expr_assignContext.class,0);
		}
		public Expr_assignContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_assignContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_expr_assign; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_assign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_assign(this);
		}
	}

	public final Expr_assignContext expr_assign(Env top) throws RecognitionException {
		Expr_assignContext _localctx = new Expr_assignContext(_ctx, getState(), top);
		enterRule(_localctx, 28, RULE_expr_assign);
		try {
			setState(321);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(313);
				((Expr_assignContext)_localctx).expr_or = expr_or(_localctx.top);
				setState(314);
				((Expr_assignContext)_localctx).a = match(T__8);
				setState(315);
				((Expr_assignContext)_localctx).ea = ((Expr_assignContext)_localctx).expr_assign = expr_assign(_localctx.top);

				        ((Expr_assignContext)_localctx).t =  ((Expr_assignContext)_localctx).expr_or.t;
				        // pc = addToStack(_localctx.top, _localctx.name, pc);
				    
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(318);
				((Expr_assignContext)_localctx).expr_or = expr_or(_localctx.top);

				        ((Expr_assignContext)_localctx).name =  ((Expr_assignContext)_localctx).expr_or.name;
				        ((Expr_assignContext)_localctx).t =  ((Expr_assignContext)_localctx).expr_or.t;
				        ((Expr_assignContext)_localctx).num =  ((Expr_assignContext)_localctx).expr_or.num;
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_orContext extends ParserRuleContext {
		public Env top;
		public String name;
		public Type t;
		public int num;
		public Expr_andContext expr_and;
		public Expr_or_tmpContext expr_or_tmp;
		public Expr_andContext expr_and() {
			return getRuleContext(Expr_andContext.class,0);
		}
		public Expr_or_tmpContext expr_or_tmp() {
			return getRuleContext(Expr_or_tmpContext.class,0);
		}
		public Expr_orContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_orContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_expr_or; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_or(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_or(this);
		}
	}

	public final Expr_orContext expr_or(Env top) throws RecognitionException {
		Expr_orContext _localctx = new Expr_orContext(_ctx, getState(), top);
		enterRule(_localctx, 30, RULE_expr_or);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(323);
			((Expr_orContext)_localctx).expr_and = expr_and(_localctx.top);
			setState(324);
			((Expr_orContext)_localctx).expr_or_tmp = expr_or_tmp(_localctx.top, ((Expr_orContext)_localctx).expr_and.num);

			        ((Expr_orContext)_localctx).name =  ((Expr_orContext)_localctx).expr_and.name;
			        ((Expr_orContext)_localctx).t =  ((Expr_orContext)_localctx).expr_and.t;
			        ((Expr_orContext)_localctx).num =  ((Expr_orContext)_localctx).expr_or_tmp.num;
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_or_tmpContext extends ParserRuleContext {
		public Env top;
		public int inh;
		public int num;
		public Expr_andContext expr_and;
		public Expr_andContext expr_and() {
			return getRuleContext(Expr_andContext.class,0);
		}
		public Expr_or_tmpContext expr_or_tmp() {
			return getRuleContext(Expr_or_tmpContext.class,0);
		}
		public Expr_or_tmpContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_or_tmpContext(ParserRuleContext parent, int invokingState, Env top, int inh) {
			super(parent, invokingState);
			this.top = top;
			this.inh = inh;
		}
		@Override public int getRuleIndex() { return RULE_expr_or_tmp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_or_tmp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_or_tmp(this);
		}
	}

	public final Expr_or_tmpContext expr_or_tmp(Env top,int inh) throws RecognitionException {
		Expr_or_tmpContext _localctx = new Expr_or_tmpContext(_ctx, getState(), top, inh);
		enterRule(_localctx, 32, RULE_expr_or_tmp);
		try {
			setState(333);
			switch (_input.LA(1)) {
			case T__25:
				enterOuterAlt(_localctx, 1);
				{
				setState(327);
				match(T__25);
				setState(328);
				((Expr_or_tmpContext)_localctx).expr_and = expr_and(_localctx.top);

				        PB[pc] = "lw $t0, 4($sp)";
				        PB[pc+1] ="or $a0, $a0, $t0";
				        PB[pc+2] ="addi $sp, $sp, 4";
				        pc += 3;
				        if(_localctx.inh>0 || ((Expr_or_tmpContext)_localctx).expr_and.num>0)
				            ((Expr_or_tmpContext)_localctx).num =  1;
				        else
				            ((Expr_or_tmpContext)_localctx).num =  0;
				    
				setState(330);
				expr_or_tmp(_localctx.top, _localctx.num);
				}
				break;
			case T__0:
			case T__4:
			case T__5:
			case T__8:
			case T__10:
				enterOuterAlt(_localctx, 2);
				{
				((Expr_or_tmpContext)_localctx).num =  _localctx.inh;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_andContext extends ParserRuleContext {
		public Env top;
		public String name;
		public Type t;
		public int num;
		public Expr_eqContext expr_eq;
		public Expr_and_tmpContext expr_and_tmp;
		public Expr_eqContext expr_eq() {
			return getRuleContext(Expr_eqContext.class,0);
		}
		public Expr_and_tmpContext expr_and_tmp() {
			return getRuleContext(Expr_and_tmpContext.class,0);
		}
		public Expr_andContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_andContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_expr_and; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_and(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_and(this);
		}
	}

	public final Expr_andContext expr_and(Env top) throws RecognitionException {
		Expr_andContext _localctx = new Expr_andContext(_ctx, getState(), top);
		enterRule(_localctx, 34, RULE_expr_and);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(335);
			((Expr_andContext)_localctx).expr_eq = expr_eq(_localctx.top);
			setState(336);
			((Expr_andContext)_localctx).expr_and_tmp = expr_and_tmp(_localctx.top, ((Expr_andContext)_localctx).expr_eq.num);

			        ((Expr_andContext)_localctx).name =  ((Expr_andContext)_localctx).expr_eq.name;
			        ((Expr_andContext)_localctx).t =  ((Expr_andContext)_localctx).expr_eq.t;
			        ((Expr_andContext)_localctx).num =  ((Expr_andContext)_localctx).expr_and_tmp.num;
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_and_tmpContext extends ParserRuleContext {
		public Env top;
		public int inh;
		public int num;
		public Expr_eqContext expr_eq;
		public Expr_eqContext expr_eq() {
			return getRuleContext(Expr_eqContext.class,0);
		}
		public Expr_and_tmpContext expr_and_tmp() {
			return getRuleContext(Expr_and_tmpContext.class,0);
		}
		public Expr_and_tmpContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_and_tmpContext(ParserRuleContext parent, int invokingState, Env top, int inh) {
			super(parent, invokingState);
			this.top = top;
			this.inh = inh;
		}
		@Override public int getRuleIndex() { return RULE_expr_and_tmp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_and_tmp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_and_tmp(this);
		}
	}

	public final Expr_and_tmpContext expr_and_tmp(Env top,int inh) throws RecognitionException {
		Expr_and_tmpContext _localctx = new Expr_and_tmpContext(_ctx, getState(), top, inh);
		enterRule(_localctx, 36, RULE_expr_and_tmp);
		try {
			setState(345);
			switch (_input.LA(1)) {
			case T__26:
				enterOuterAlt(_localctx, 1);
				{
				setState(339);
				match(T__26);
				setState(340);
				((Expr_and_tmpContext)_localctx).expr_eq = expr_eq(_localctx.top);

				        PB[pc] = "lw $t0, 4($sp)";
				        PB[pc+1] ="and $a0, $a0, $t0";
				        PB[pc+2] ="addi $sp, $sp, 4";
				        pc += 3;
				        if(_localctx.inh>0 && ((Expr_and_tmpContext)_localctx).expr_eq.num>0)
				            ((Expr_and_tmpContext)_localctx).num =  1;
				        else
				            ((Expr_and_tmpContext)_localctx).num =  0;
				    
				setState(342);
				expr_and_tmp(_localctx.top, _localctx.num);
				}
				break;
			case T__0:
			case T__4:
			case T__5:
			case T__8:
			case T__10:
			case T__25:
				enterOuterAlt(_localctx, 2);
				{
				((Expr_and_tmpContext)_localctx).num =  _localctx.inh;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_eqContext extends ParserRuleContext {
		public Env top;
		public String name;
		public Type t;
		public int num;
		public Expr_cmpContext expr_cmp;
		public Expr_eq_tmpContext expr_eq_tmp;
		public Expr_cmpContext expr_cmp() {
			return getRuleContext(Expr_cmpContext.class,0);
		}
		public Expr_eq_tmpContext expr_eq_tmp() {
			return getRuleContext(Expr_eq_tmpContext.class,0);
		}
		public Expr_eqContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_eqContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_expr_eq; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_eq(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_eq(this);
		}
	}

	public final Expr_eqContext expr_eq(Env top) throws RecognitionException {
		Expr_eqContext _localctx = new Expr_eqContext(_ctx, getState(), top);
		enterRule(_localctx, 38, RULE_expr_eq);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(347);
			((Expr_eqContext)_localctx).expr_cmp = expr_cmp(_localctx.top);
			setState(348);
			((Expr_eqContext)_localctx).expr_eq_tmp = expr_eq_tmp(_localctx.top, ((Expr_eqContext)_localctx).expr_cmp.num);

			        ((Expr_eqContext)_localctx).name =  ((Expr_eqContext)_localctx).expr_cmp.name; 
			        ((Expr_eqContext)_localctx).t =  ((Expr_eqContext)_localctx).expr_cmp.t;
			        ((Expr_eqContext)_localctx).num =  ((Expr_eqContext)_localctx).expr_eq_tmp.num;
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_eq_tmpContext extends ParserRuleContext {
		public Env top;
		public int inh;
		public int num;
		public Token op;
		public Expr_cmpContext expr_cmp() {
			return getRuleContext(Expr_cmpContext.class,0);
		}
		public Expr_eq_tmpContext expr_eq_tmp() {
			return getRuleContext(Expr_eq_tmpContext.class,0);
		}
		public Expr_eq_tmpContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_eq_tmpContext(ParserRuleContext parent, int invokingState, Env top, int inh) {
			super(parent, invokingState);
			this.top = top;
			this.inh = inh;
		}
		@Override public int getRuleIndex() { return RULE_expr_eq_tmp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_eq_tmp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_eq_tmp(this);
		}
	}

	public final Expr_eq_tmpContext expr_eq_tmp(Env top,int inh) throws RecognitionException {
		Expr_eq_tmpContext _localctx = new Expr_eq_tmpContext(_ctx, getState(), top, inh);
		enterRule(_localctx, 40, RULE_expr_eq_tmp);
		int _la;
		try {
			setState(357);
			switch (_input.LA(1)) {
			case T__27:
			case T__28:
				enterOuterAlt(_localctx, 1);
				{
				setState(351);
				((Expr_eq_tmpContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__27 || _la==T__28) ) {
					((Expr_eq_tmpContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(352);
				expr_cmp(_localctx.top);

				        if((((Expr_eq_tmpContext)_localctx).op!=null?((Expr_eq_tmpContext)_localctx).op.getText():null).equals("==")){
				            ((Expr_eq_tmpContext)_localctx).num =  1;
				            PB[pc] = "lw $t0, 4($sp)";
				            PB[pc+1] = "sub $a0, $t0, $a0";
				            PB[pc+2] = "li $t1, 1";
				            PB[pc+3] = "move $t2, $a0";
				            PB[pc+4] = "movz $a0,$t1, $a0";
				            PB[pc+5] = "movn $a0,$0, $t2";
				            PB[pc+6] = "addiu $sp, $sp, 4";
				            pc += 7;
				        }
				        else{
				            ((Expr_eq_tmpContext)_localctx).num =  0;
				            PB[pc] = "lw $t0, 4($sp)";
				            PB[pc+1] = "sub $a0, $t0, $a0";
				            PB[pc+2] = "li $t1, 1";
				            PB[pc+3] = "move $t2, $a0";
				            PB[pc+4] = "movz $a0,$0, $a0";
				            PB[pc+5] = "movn $a0,$t1, $t2";
				            PB[pc+6] = "addiu $sp, $sp, 4";
				            pc += 7;
				        }
				    
				setState(354);
				expr_eq_tmp(_localctx.top, _localctx.num);
				}
				break;
			case T__0:
			case T__4:
			case T__5:
			case T__8:
			case T__10:
			case T__25:
			case T__26:
				enterOuterAlt(_localctx, 2);
				{
				((Expr_eq_tmpContext)_localctx).num =  _localctx.inh;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_cmpContext extends ParserRuleContext {
		public Env top;
		public String name;
		public Type t;
		public int num;
		public Expr_addContext expr_add;
		public Expr_cmp_tmpContext expr_cmp_tmp;
		public Expr_addContext expr_add() {
			return getRuleContext(Expr_addContext.class,0);
		}
		public Expr_cmp_tmpContext expr_cmp_tmp() {
			return getRuleContext(Expr_cmp_tmpContext.class,0);
		}
		public Expr_cmpContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_cmpContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_expr_cmp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_cmp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_cmp(this);
		}
	}

	public final Expr_cmpContext expr_cmp(Env top) throws RecognitionException {
		Expr_cmpContext _localctx = new Expr_cmpContext(_ctx, getState(), top);
		enterRule(_localctx, 42, RULE_expr_cmp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(359);
			((Expr_cmpContext)_localctx).expr_add = expr_add(_localctx.top);
			setState(360);
			((Expr_cmpContext)_localctx).expr_cmp_tmp = expr_cmp_tmp(_localctx.top, ((Expr_cmpContext)_localctx).expr_add.num);

			        ((Expr_cmpContext)_localctx).name =  ((Expr_cmpContext)_localctx).expr_add.name;
			        ((Expr_cmpContext)_localctx).t =  ((Expr_cmpContext)_localctx).expr_add.t;
			        ((Expr_cmpContext)_localctx).num =  ((Expr_cmpContext)_localctx).expr_cmp_tmp.num;
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_cmp_tmpContext extends ParserRuleContext {
		public Env top;
		public int inh;
		public int num;
		public Token op;
		public Expr_addContext expr_add() {
			return getRuleContext(Expr_addContext.class,0);
		}
		public Expr_cmp_tmpContext expr_cmp_tmp() {
			return getRuleContext(Expr_cmp_tmpContext.class,0);
		}
		public Expr_cmp_tmpContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_cmp_tmpContext(ParserRuleContext parent, int invokingState, Env top, int inh) {
			super(parent, invokingState);
			this.top = top;
			this.inh = inh;
		}
		@Override public int getRuleIndex() { return RULE_expr_cmp_tmp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_cmp_tmp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_cmp_tmp(this);
		}
	}

	public final Expr_cmp_tmpContext expr_cmp_tmp(Env top,int inh) throws RecognitionException {
		Expr_cmp_tmpContext _localctx = new Expr_cmp_tmpContext(_ctx, getState(), top, inh);
		enterRule(_localctx, 44, RULE_expr_cmp_tmp);
		int _la;
		try {
			setState(369);
			switch (_input.LA(1)) {
			case T__13:
			case T__16:
				enterOuterAlt(_localctx, 1);
				{
				setState(363);
				((Expr_cmp_tmpContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__13 || _la==T__16) ) {
					((Expr_cmp_tmpContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(364);
				expr_add(_localctx.top);

				        if((((Expr_cmp_tmpContext)_localctx).op!=null?((Expr_cmp_tmpContext)_localctx).op.getText():null).equals(">")){
				            ((Expr_cmp_tmpContext)_localctx).num =  1;
				            PB[pc] = "lw $t0, 4($sp)";
				            PB[pc+1] = "sub $a0, $t0, $a0";
				            PB[pc+2] = "slt $a0, $0, $a0";
				            PB[pc+3] = "addiu $sp, $sp, 4";
				            pc += 4;
				        }
				        else{
				            ((Expr_cmp_tmpContext)_localctx).num =  0;
				            PB[pc] ="lw $t0, 4($sp)";
				            PB[pc+1] ="slt $a0, $t0, $a0";
				            PB[pc+2] = "addiu $sp, $sp, 4";
				            pc += 3;
				        }
				    
				setState(366);
				expr_cmp_tmp(_localctx.top, _localctx.num);
				}
				break;
			case T__0:
			case T__4:
			case T__5:
			case T__8:
			case T__10:
			case T__25:
			case T__26:
			case T__27:
			case T__28:
				enterOuterAlt(_localctx, 2);
				{
				((Expr_cmp_tmpContext)_localctx).num =  _localctx.inh;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_addContext extends ParserRuleContext {
		public Env top;
		public String name;
		public Type t;
		public int num;
		public Expr_multContext expr_mult;
		public Expr_add_tmpContext expr_add_tmp;
		public Expr_multContext expr_mult() {
			return getRuleContext(Expr_multContext.class,0);
		}
		public Expr_add_tmpContext expr_add_tmp() {
			return getRuleContext(Expr_add_tmpContext.class,0);
		}
		public Expr_addContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_addContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_expr_add; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_add(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_add(this);
		}
	}

	public final Expr_addContext expr_add(Env top) throws RecognitionException {
		Expr_addContext _localctx = new Expr_addContext(_ctx, getState(), top);
		enterRule(_localctx, 46, RULE_expr_add);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(371);
			((Expr_addContext)_localctx).expr_mult = expr_mult(_localctx.top);
			setState(372);
			((Expr_addContext)_localctx).expr_add_tmp = expr_add_tmp(_localctx.top, ((Expr_addContext)_localctx).expr_mult.num);

			        ((Expr_addContext)_localctx).name =  ((Expr_addContext)_localctx).expr_mult.name;
			        ((Expr_addContext)_localctx).t =  ((Expr_addContext)_localctx).expr_mult.t;
			        ((Expr_addContext)_localctx).num =  ((Expr_addContext)_localctx).expr_add_tmp.num;
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_add_tmpContext extends ParserRuleContext {
		public Env top;
		public int inh;
		public int num;
		public Token op;
		public Expr_multContext expr_mult;
		public Expr_multContext expr_mult() {
			return getRuleContext(Expr_multContext.class,0);
		}
		public Expr_add_tmpContext expr_add_tmp() {
			return getRuleContext(Expr_add_tmpContext.class,0);
		}
		public Expr_add_tmpContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_add_tmpContext(ParserRuleContext parent, int invokingState, Env top, int inh) {
			super(parent, invokingState);
			this.top = top;
			this.inh = inh;
		}
		@Override public int getRuleIndex() { return RULE_expr_add_tmp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_add_tmp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_add_tmp(this);
		}
	}

	public final Expr_add_tmpContext expr_add_tmp(Env top,int inh) throws RecognitionException {
		Expr_add_tmpContext _localctx = new Expr_add_tmpContext(_ctx, getState(), top, inh);
		enterRule(_localctx, 48, RULE_expr_add_tmp);
		int _la;
		try {
			setState(381);
			switch (_input.LA(1)) {
			case T__29:
			case T__30:
				enterOuterAlt(_localctx, 1);
				{
				setState(375);
				((Expr_add_tmpContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__29 || _la==T__30) ) {
					((Expr_add_tmpContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(376);
				((Expr_add_tmpContext)_localctx).expr_mult = expr_mult(_localctx.top);

				        if((((Expr_add_tmpContext)_localctx).op!=null?((Expr_add_tmpContext)_localctx).op.getText():null).equals("+")){
				            ((Expr_add_tmpContext)_localctx).num =  _localctx.inh + ((Expr_add_tmpContext)_localctx).expr_mult.num;
				            PB[pc] = "lw $t1, 4($sp)";
				            PB[pc+1] = "add $a0, $a0, $t1";
				            PB[pc+2] = "addiu $sp, $sp, 4";
				            pc += 3;
				        }
				        else{
				            ((Expr_add_tmpContext)_localctx).num =  _localctx.inh - ((Expr_add_tmpContext)_localctx).expr_mult.num;
				            PB[pc] = "lw $t1, 4($sp)";
				            PB[pc+1] = "sub $a0, $a0, $t1";
				            PB[pc+2] = "addiu $sp, $sp, 4";
				            pc += 3;
				        }
				    
				setState(378);
				expr_add_tmp(_localctx.top, _localctx.num);
				}
				break;
			case T__0:
			case T__4:
			case T__5:
			case T__8:
			case T__10:
			case T__13:
			case T__16:
			case T__25:
			case T__26:
			case T__27:
			case T__28:
				enterOuterAlt(_localctx, 2);
				{
				((Expr_add_tmpContext)_localctx).num =  _localctx.inh;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_multContext extends ParserRuleContext {
		public Env top;
		public String name;
		public Type t;
		public int num;
		public Expr_unContext expr_un;
		public Expr_mult_tmpContext expr_mult_tmp;
		public Expr_unContext expr_un() {
			return getRuleContext(Expr_unContext.class,0);
		}
		public Expr_mult_tmpContext expr_mult_tmp() {
			return getRuleContext(Expr_mult_tmpContext.class,0);
		}
		public Expr_multContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_multContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_expr_mult; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_mult(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_mult(this);
		}
	}

	public final Expr_multContext expr_mult(Env top) throws RecognitionException {
		Expr_multContext _localctx = new Expr_multContext(_ctx, getState(), top);
		enterRule(_localctx, 50, RULE_expr_mult);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(383);
			((Expr_multContext)_localctx).expr_un = expr_un(_localctx.top);
			setState(384);
			((Expr_multContext)_localctx).expr_mult_tmp = expr_mult_tmp(_localctx.top, ((Expr_multContext)_localctx).expr_un.num);

			        ((Expr_multContext)_localctx).name =  ((Expr_multContext)_localctx).expr_un.name;
			        ((Expr_multContext)_localctx).t =  ((Expr_multContext)_localctx).expr_un.t;
			        ((Expr_multContext)_localctx).num =  ((Expr_multContext)_localctx).expr_mult_tmp.num;
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_mult_tmpContext extends ParserRuleContext {
		public Env top;
		public int inh;
		public int num;
		public Token op;
		public Expr_unContext expr_un;
		public Expr_unContext expr_un() {
			return getRuleContext(Expr_unContext.class,0);
		}
		public Expr_mult_tmpContext expr_mult_tmp() {
			return getRuleContext(Expr_mult_tmpContext.class,0);
		}
		public Expr_mult_tmpContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_mult_tmpContext(ParserRuleContext parent, int invokingState, Env top, int inh) {
			super(parent, invokingState);
			this.top = top;
			this.inh = inh;
		}
		@Override public int getRuleIndex() { return RULE_expr_mult_tmp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_mult_tmp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_mult_tmp(this);
		}
	}

	public final Expr_mult_tmpContext expr_mult_tmp(Env top,int inh) throws RecognitionException {
		Expr_mult_tmpContext _localctx = new Expr_mult_tmpContext(_ctx, getState(), top, inh);
		enterRule(_localctx, 52, RULE_expr_mult_tmp);
		int _la;
		try {
			setState(393);
			switch (_input.LA(1)) {
			case T__14:
			case T__31:
				enterOuterAlt(_localctx, 1);
				{
				setState(387);
				((Expr_mult_tmpContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__14 || _la==T__31) ) {
					((Expr_mult_tmpContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(388);
				((Expr_mult_tmpContext)_localctx).expr_un = expr_un(_localctx.top);

				        if((((Expr_mult_tmpContext)_localctx).op!=null?((Expr_mult_tmpContext)_localctx).op.getText():null).equals("*")){
				            ((Expr_mult_tmpContext)_localctx).num =  _localctx.inh * ((Expr_mult_tmpContext)_localctx).expr_un.num;
				            PB[pc] = "lw $t1, 4($sp)";
				            PB[pc+1] = "mul $a0, $a0, $t1";
				            PB[pc+2] = "addiu $sp, $sp, 4";
				            pc += 3;
				        }
				        else{
				            ((Expr_mult_tmpContext)_localctx).num =  _localctx.inh / ((Expr_mult_tmpContext)_localctx).expr_un.num;
				            PB[pc] = "lw $t1, 4($sp)";
				            PB[pc+1] = "div $a0, $t1, $a0";
				            PB[pc+2] = "addiu $sp, $sp, 4";
				            pc += 3;
				        }
				    
				setState(390);
				expr_mult_tmp(_localctx.top, _localctx.num);
				}
				break;
			case T__0:
			case T__4:
			case T__5:
			case T__8:
			case T__10:
			case T__13:
			case T__16:
			case T__25:
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__30:
				enterOuterAlt(_localctx, 2);
				{
				((Expr_mult_tmpContext)_localctx).num =  _localctx.inh;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_unContext extends ParserRuleContext {
		public Env top;
		public String name;
		public Type t;
		public int num;
		public Token op;
		public Expr_unContext eu;
		public Expr_unContext expr_un;
		public Expr_memContext expr_mem;
		public Expr_unContext expr_un() {
			return getRuleContext(Expr_unContext.class,0);
		}
		public Expr_memContext expr_mem() {
			return getRuleContext(Expr_memContext.class,0);
		}
		public Expr_unContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_unContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_expr_un; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_un(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_un(this);
		}
	}

	public final Expr_unContext expr_un(Env top) throws RecognitionException {
		Expr_unContext _localctx = new Expr_unContext(_ctx, getState(), top);
		enterRule(_localctx, 54, RULE_expr_un);
		int _la;
		try {
			setState(402);
			switch (_input.LA(1)) {
			case T__30:
			case T__32:
				enterOuterAlt(_localctx, 1);
				{
				setState(395);
				((Expr_unContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__30 || _la==T__32) ) {
					((Expr_unContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(396);
				((Expr_unContext)_localctx).eu = ((Expr_unContext)_localctx).expr_un = expr_un(_localctx.top);

				        ((Expr_unContext)_localctx).t =  _localctx.t; 
				        if((((Expr_unContext)_localctx).op!=null?((Expr_unContext)_localctx).op.getText():null).equals("!")){
				            if(((Expr_unContext)_localctx).eu.num == 0){
				                PB[pc] = "lw $t1, 4($sp)";
				                PB[pc+1] = "not $a0, $t1";
				                pc += 2;
				            }
				            else{
				                PB[pc] = "lw $t1, 4($sp)";
				                PB[pc+1] = "neg $a0, $a0, $t1";
				                pc += 2;
				            }
				        }
				        else if(((Expr_unContext)_localctx).eu.num != 0)     
				            ((Expr_unContext)_localctx).num =  -((Expr_unContext)_localctx).eu.num;
				        else
				            ((Expr_unContext)_localctx).num =  0;
				    
				}
				break;
			case T__2:
			case T__9:
			case T__35:
			case CONST_NUM:
			case CONST_CHAR:
			case CONST_STR:
			case CONST_FUNC:
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(399);
				((Expr_unContext)_localctx).expr_mem = expr_mem(_localctx.top);

				        ((Expr_unContext)_localctx).name =  ((Expr_unContext)_localctx).expr_mem.name;
				        ((Expr_unContext)_localctx).t =  ((Expr_unContext)_localctx).expr_mem.t;
				        ((Expr_unContext)_localctx).num =  ((Expr_unContext)_localctx).expr_mem.num;
				    
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_memContext extends ParserRuleContext {
		public Env top;
		public String name;
		public Type t;
		public int num;
		public boolean flag;
		public Type temp;
		public Expr_otherContext expr_other;
		public Expr_mem_tmpContext expr_mem_tmp;
		public Expr_otherContext expr_other() {
			return getRuleContext(Expr_otherContext.class,0);
		}
		public Expr_mem_tmpContext expr_mem_tmp() {
			return getRuleContext(Expr_mem_tmpContext.class,0);
		}
		public Expr_memContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_memContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_expr_mem; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_mem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_mem(this);
		}
	}

	public final Expr_memContext expr_mem(Env top) throws RecognitionException {
		Expr_memContext _localctx = new Expr_memContext(_ctx, getState(), top);
		enterRule(_localctx, 56, RULE_expr_mem);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(404);
			((Expr_memContext)_localctx).expr_other = expr_other(_localctx.top);
			setState(405);
			((Expr_memContext)_localctx).expr_mem_tmp = expr_mem_tmp(_localctx.top);
			   
			        ((Expr_memContext)_localctx).num =  ((Expr_memContext)_localctx).expr_other.num;
			        ((Expr_memContext)_localctx).flag =  false;
			        ((Expr_memContext)_localctx).name =  ((Expr_memContext)_localctx).expr_mem_tmp.indexName; 
			        ((Expr_memContext)_localctx).t =  ((Expr_memContext)_localctx).expr_other.t;
			        if(((Expr_memContext)_localctx).expr_mem_tmp.sharp){
			            ((Expr_memContext)_localctx).t =  ((Expr_memContext)_localctx).expr_other.t.t.get(((Expr_memContext)_localctx).expr_mem_tmp.num).getR();
			        }
			        else if(((Expr_memContext)_localctx).expr_mem_tmp.dot)
			            ((Expr_memContext)_localctx).t =  ((Expr_memContext)_localctx).expr_other.t.contain(((Expr_memContext)_localctx).expr_mem_tmp.indexName);

			        else if(((Expr_memContext)_localctx).expr_mem_tmp.func){
			            ((Expr_memContext)_localctx).flag =  true;
			            if(((Expr_memContext)_localctx).expr_other.name.equals("head")){
			                ((Expr_memContext)_localctx).t =  ((Expr_memContext)_localctx).expr_mem_tmp.headType;
			                pc = addToStack(_localctx.top, ((Expr_memContext)_localctx).expr_mem_tmp.name, pc);
			            }
			            else if(((Expr_memContext)_localctx).expr_other.name.equals("tail")){
			                ((Expr_memContext)_localctx).t =  ((Expr_memContext)_localctx).expr_mem_tmp.tailType;
			                pc = addToStack(_localctx.top, ((Expr_memContext)_localctx).expr_mem_tmp.name, pc);
			            }
			            else if(((Expr_memContext)_localctx).expr_other.name.equals("size")){
			                ((Expr_memContext)_localctx).t =  ((Expr_memContext)_localctx).expr_other.t;
			                pc = addToStack(((Expr_memContext)_localctx).expr_mem_tmp.args.get(0).getR().t.size(), pc);
			            }
			            else if(((Expr_memContext)_localctx).expr_other.name.equals("read"))
			                for(int i=0;i<((Expr_memContext)_localctx).expr_mem_tmp.num;i++){
			                    _localctx.t.addType("", new Type("char"));
			                    PB[pc] = "li $v0, 12";
			                    PB[pc+1] = "syscall";
			                    PB[pc+2] = "sw $v0, 0($sp)";
			                    PB[pc+3] = "addiu $sp, $sp, -4";
			                    pc += 4;
			                }
			            else if(((Expr_memContext)_localctx).expr_other.name.equals("write")){
			                if(isAccFull){
			                    PB[pc] = "sw $a0, 0($sp)";
			                    PB[pc+1] = "addiu $sp, $sp, -4";
			                    isAccFull = false;
			                    pc += 2;
			                }
			                if(_localctx.top.getAdr(((Expr_memContext)_localctx).expr_mem_tmp.name)!=-1){
			                    PB[pc] = "lw $a0, -"+_localctx.top.getAdr(((Expr_memContext)_localctx).expr_mem_tmp.name)+"($fp)";
			                    PB[pc+1] = "li $v0, 11";
			                    PB[pc+2] = "syscall";
			                    pc += 3;
			                    for(int i=1; i<((Expr_memContext)_localctx).expr_mem_tmp.args.get(0).getR().t.size(); i++){
			                        PB[pc] = "lw $a0, -"+(_localctx.top.getAdr(((Expr_memContext)_localctx).expr_mem_tmp.name)+(i*4))+"($fp)";
			                        PB[pc+1] = "li $v0, 11";
			                        PB[pc+2] = "syscall";
			                        pc += 3;
			                    }
			                }else{
			                    int i;
			                    for(i=0; i<((Expr_memContext)_localctx).expr_mem_tmp.args.get(0).getR().t.size(); i++){
			                        PB[pc] = "lw $a0, "+((((Expr_memContext)_localctx).expr_mem_tmp.args.get(0).getR().t.size()-i)*4)+"($sp)";
			                        PB[pc+1] = "li $v0, 11";
			                        PB[pc+2] = "syscall";
			                        pc += 3;
			                    }
			                    PB[pc] = "addiu $sp, $sp, "+(i*4);
			                    pc++;
			                }
			            }
			        }
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_mem_tmpContext extends ParserRuleContext {
		public Env top;
		public String name;
		public String indexName;
		public boolean dot;
		public Type headType;
		public Type tailType;
		public int num;
		public boolean func;
		public boolean sharp;
		public ArrayList<Pair<String, Type>> args;
		public Expr_otherContext expr_other;
		public Token ID;
		public ExprContext e1;
		public ExprContext e2;
		public Expr_mem_tmpContext expr_mem_tmp() {
			return getRuleContext(Expr_mem_tmpContext.class,0);
		}
		public Expr_otherContext expr_other() {
			return getRuleContext(Expr_otherContext.class,0);
		}
		public TerminalNode ID() { return getToken(XParser.ID, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Expr_mem_tmpContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_mem_tmpContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_expr_mem_tmp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_mem_tmp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_mem_tmp(this);
		}
	}

	public final Expr_mem_tmpContext expr_mem_tmp(Env top) throws RecognitionException {
		Expr_mem_tmpContext _localctx = new Expr_mem_tmpContext(_ctx, getState(), top);
		enterRule(_localctx, 58, RULE_expr_mem_tmp);
		int _la;
		try {
			setState(436);
			switch (_input.LA(1)) {
			case T__2:
			case T__33:
			case T__34:
				enterOuterAlt(_localctx, 1);
				{

				        ((Expr_mem_tmpContext)_localctx).dot = false;
				        ((Expr_mem_tmpContext)_localctx).headType =  new Type("nil");
				        ((Expr_mem_tmpContext)_localctx).tailType =  new Type("nil");
				        ((Expr_mem_tmpContext)_localctx).func = false;
				        ((Expr_mem_tmpContext)_localctx).sharp = false;
				        ((Expr_mem_tmpContext)_localctx).args =  new ArrayList<Pair<String, Type>>();
				        ((Expr_mem_tmpContext)_localctx).num =  0;
				    
				setState(431);
				switch (_input.LA(1)) {
				case T__33:
					{
					setState(409);
					match(T__33);
					setState(410);
					((Expr_mem_tmpContext)_localctx).expr_other = expr_other(_localctx.top);
					((Expr_mem_tmpContext)_localctx).num = ((Expr_mem_tmpContext)_localctx).expr_other.num;((Expr_mem_tmpContext)_localctx).sharp = true;
					}
					break;
				case T__34:
					{
					setState(413);
					match(T__34);
					setState(414);
					((Expr_mem_tmpContext)_localctx).ID = match(ID);
					((Expr_mem_tmpContext)_localctx).indexName =  (((Expr_mem_tmpContext)_localctx).ID!=null?((Expr_mem_tmpContext)_localctx).ID.getText():null);((Expr_mem_tmpContext)_localctx).dot = true;
					}
					break;
				case T__2:
					{
					setState(416);
					match(T__2);
					setState(428);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__9) | (1L << T__30) | (1L << T__32) | (1L << T__35) | (1L << CONST_NUM) | (1L << CONST_CHAR) | (1L << CONST_STR) | (1L << CONST_FUNC) | (1L << ID))) != 0)) {
						{
						setState(417);
						((Expr_mem_tmpContext)_localctx).e1 = expr(_localctx.top);

						        ((Expr_mem_tmpContext)_localctx).name =  (((Expr_mem_tmpContext)_localctx).e1!=null?_input.getText(((Expr_mem_tmpContext)_localctx).e1.start,((Expr_mem_tmpContext)_localctx).e1.stop):null);
						        if(((Expr_mem_tmpContext)_localctx).e1.t!=null && ((Expr_mem_tmpContext)_localctx).e1.t.name.equals("list") && ((Expr_mem_tmpContext)_localctx).e1.t.t.size()!=0){
						            ((Expr_mem_tmpContext)_localctx).headType =  ((Expr_mem_tmpContext)_localctx).e1.t;
						            ((Expr_mem_tmpContext)_localctx).tailType =  ((Expr_mem_tmpContext)_localctx).e1.t;
						        }
						        ((Expr_mem_tmpContext)_localctx).num =  ((Expr_mem_tmpContext)_localctx).e1.num;
						        Pair newPair = new Pair("", ((Expr_mem_tmpContext)_localctx).e1.t);
						        _localctx.args.add(newPair);
						    
						setState(425);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==T__4) {
							{
							{
							setState(419);
							match(T__4);
							setState(420);
							((Expr_mem_tmpContext)_localctx).e2 = expr(_localctx.top);

							        ((Expr_mem_tmpContext)_localctx).tailType =  ((Expr_mem_tmpContext)_localctx).e2.t;
							        Pair newPair2 = new Pair("", ((Expr_mem_tmpContext)_localctx).e2.t);
							        _localctx.args.add(newPair2);
							    
							}
							}
							setState(427);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(430);
					match(T__5);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				((Expr_mem_tmpContext)_localctx).func = true;
				setState(434);
				expr_mem_tmp(_localctx.top);
				}
				break;
			case T__0:
			case T__4:
			case T__5:
			case T__8:
			case T__10:
			case T__13:
			case T__14:
			case T__16:
			case T__25:
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__30:
			case T__31:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_otherContext extends ParserRuleContext {
		public Env top;
		public String name;
		public Type t;
		public boolean isList;
		public boolean undef;
		public int num;
		public Type temp;
		public Token CONST_NUM;
		public Token CONST_CHAR;
		public Token CONST_STR;
		public Token CONST_FUNC;
		public Token ID;
		public Token a;
		public ExprContext e1;
		public ExprContext expr;
		public ExprContext e2;
		public TerminalNode CONST_NUM() { return getToken(XParser.CONST_NUM, 0); }
		public TerminalNode CONST_CHAR() { return getToken(XParser.CONST_CHAR, 0); }
		public TerminalNode CONST_STR() { return getToken(XParser.CONST_STR, 0); }
		public TerminalNode CONST_FUNC() { return getToken(XParser.CONST_FUNC, 0); }
		public TerminalNode ID() { return getToken(XParser.ID, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Expr_otherContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Expr_otherContext(ParserRuleContext parent, int invokingState, Env top) {
			super(parent, invokingState);
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_expr_other; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterExpr_other(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitExpr_other(this);
		}
	}

	public final Expr_otherContext expr_other(Env top) throws RecognitionException {
		Expr_otherContext _localctx = new Expr_otherContext(_ctx, getState(), top);
		enterRule(_localctx, 60, RULE_expr_other);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{

			        ((Expr_otherContext)_localctx).isList =  false;
			        ((Expr_otherContext)_localctx).name = "";
			        ((Expr_otherContext)_localctx).undef =  false;
			        ((Expr_otherContext)_localctx).num =  0;
			    
			setState(473);
			switch (_input.LA(1)) {
			case CONST_NUM:
				{
				setState(439);
				((Expr_otherContext)_localctx).CONST_NUM = match(CONST_NUM);

				        ((Expr_otherContext)_localctx).num =  Integer.parseInt((((Expr_otherContext)_localctx).CONST_NUM!=null?((Expr_otherContext)_localctx).CONST_NUM.getText():null));
				        ((Expr_otherContext)_localctx).t =  new Type("int");
				        pc = addToStack(_localctx.num, pc);
				    
				}
				break;
			case CONST_CHAR:
				{
				setState(441);
				((Expr_otherContext)_localctx).CONST_CHAR = match(CONST_CHAR);

				        ((Expr_otherContext)_localctx).t =  new Type("char");
				        pc = addToStack((((Expr_otherContext)_localctx).CONST_CHAR!=null?((Expr_otherContext)_localctx).CONST_CHAR.getText():null).charAt(1), pc);
				    
				}
				break;
			case CONST_STR:
				{
				setState(443);
				((Expr_otherContext)_localctx).CONST_STR = match(CONST_STR);

				        ((Expr_otherContext)_localctx).t =  new Type("list");
				        ((Expr_otherContext)_localctx).temp =  new Type("char");
				        for(int i=0;i<(((Expr_otherContext)_localctx).CONST_STR!=null?((Expr_otherContext)_localctx).CONST_STR.getText():null).length()-2;i++){
				            _localctx.t.addType("", _localctx.temp);
				            pc = addToStack((((Expr_otherContext)_localctx).CONST_STR!=null?((Expr_otherContext)_localctx).CONST_STR.getText():null).charAt(i+1), pc);
				        }
				        ((Expr_otherContext)_localctx).isList =  true;
				    
				}
				break;
			case CONST_FUNC:
				{
				setState(445);
				((Expr_otherContext)_localctx).CONST_FUNC = match(CONST_FUNC);

				        ((Expr_otherContext)_localctx).name =  (((Expr_otherContext)_localctx).CONST_FUNC!=null?((Expr_otherContext)_localctx).CONST_FUNC.getText():null);
				        switch((((Expr_otherContext)_localctx).CONST_FUNC!=null?((Expr_otherContext)_localctx).CONST_FUNC.getText():null)){
				            case("head"):
				                ((Expr_otherContext)_localctx).t =  new Type("head");
				            break;
				            case("tail"):
				                ((Expr_otherContext)_localctx).t =  new Type("tail");
				            break;
				            case("size"):
				                ((Expr_otherContext)_localctx).t =  new Type("int");
				            break;
				            case("read"):
				                ((Expr_otherContext)_localctx).t =  new Type("list");
				                ((Expr_otherContext)_localctx).isList =  true;
				            break;
				            case("write"):
				                ((Expr_otherContext)_localctx).t =  new Type("void");
				            break;
				        }
				        
				    
				}
				break;
			case ID:
				{
				setState(447);
				((Expr_otherContext)_localctx).ID = match(ID);

				        ((Expr_otherContext)_localctx).name =  (((Expr_otherContext)_localctx).ID!=null?((Expr_otherContext)_localctx).ID.getText():null); 
				        if(prog.getType(_localctx.top, _localctx.name) == null){
				            ((Expr_otherContext)_localctx).undef =  true;
				            ((Expr_otherContext)_localctx).t =  new Type("nil");
				        }
				        else{
				            ((Expr_otherContext)_localctx).t =  prog.getType(_localctx.top, _localctx.name);
				            // pc = addToStack(_localctx.top, _localctx.name, pc);
				            if(_localctx.t.name.equals("list"))
				        	    ((Expr_otherContext)_localctx).isList =  true;
				        }
				    
				}
				break;
			case T__9:
				{
				((Expr_otherContext)_localctx).t =  new Type("list");
				setState(450);
				((Expr_otherContext)_localctx).a = match(T__9);
				setState(462);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__9) | (1L << T__30) | (1L << T__32) | (1L << T__35) | (1L << CONST_NUM) | (1L << CONST_CHAR) | (1L << CONST_STR) | (1L << CONST_FUNC) | (1L << ID))) != 0)) {
					{
					setState(451);
					((Expr_otherContext)_localctx).e1 = ((Expr_otherContext)_localctx).expr = expr(_localctx.top);
					_localctx.t.addType("", ((Expr_otherContext)_localctx).e1.t);
					setState(459);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__4) {
						{
						{
						setState(453);
						match(T__4);
						setState(454);
						((Expr_otherContext)_localctx).e2 = ((Expr_otherContext)_localctx).expr = expr(_localctx.top);
						_localctx.t.addType("", ((Expr_otherContext)_localctx).e2.t);
						}
						}
						setState(461);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(464);
				match(T__10);
				((Expr_otherContext)_localctx).isList =  true;
				}
				break;
			case T__35:
				{
				setState(466);
				((Expr_otherContext)_localctx).a = match(T__35);
				((Expr_otherContext)_localctx).t =  new Type("nil");
				}
				break;
			case T__2:
				{
				setState(468);
				((Expr_otherContext)_localctx).a = match(T__2);
				setState(469);
				((Expr_otherContext)_localctx).expr = expr(_localctx.top);
				setState(470);
				match(T__5);
				((Expr_otherContext)_localctx).t =  ((Expr_otherContext)_localctx).expr.t;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_stmContext extends ParserRuleContext {
		public Type ret;
		public Env top;
		public String label1;
		public String label2;
		public int whileJmp;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public While_stmContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public While_stmContext(ParserRuleContext parent, int invokingState, Type ret, Env top) {
			super(parent, invokingState);
			this.ret = ret;
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_while_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterWhile_stm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitWhile_stm(this);
		}
	}

	public final While_stmContext while_stm(Type ret,Env top) throws RecognitionException {
		While_stmContext _localctx = new While_stmContext(_ctx, getState(), ret, top);
		enterRule(_localctx, 62, RULE_while_stm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			breakAdr.add(-1);
			setState(476);
			match(T__36);

			        ((While_stmContext)_localctx).label1 =  labelGenerator(3); 
			        PB[pc] = _localctx.label1+":";
			        pc++;
			    
			setState(478);
			match(T__2);
			setState(479);
			expr(_localctx.top);

			        ((While_stmContext)_localctx).whileJmp =  pc;
			        pc++;
			        isBreak = false;
			    
			setState(481);
			match(T__5);
			setState(482);
			statement(_localctx.ret, _localctx.top);

			        ((While_stmContext)_localctx).label2 =  labelGenerator(3);
			        PB[_localctx.whileJmp] = "blez $a0, " + _localctx.label2;
			        PB[pc] = "j " + _localctx.label1;
			        PB[pc+1] = _localctx.label2+ ":";
			        pc += 2;

			        while(breakAdr.get(breakAdr.size()-1) != -1){
			            PB[breakAdr.get(breakAdr.size()-1)] = "j " + _localctx.label2;
			            breakAdr.remove(breakAdr.get(breakAdr.size()-1));
			        }
			        breakAdr.remove(breakAdr.get(breakAdr.size()-1));
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreach_stmContext extends ParserRuleContext {
		public Type ret;
		public Env top;
		public int size;
		public int i;
		public TypeContext type;
		public Token ID;
		public ExprContext expr;
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(XParser.ID, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public Foreach_stmContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Foreach_stmContext(ParserRuleContext parent, int invokingState, Type ret, Env top) {
			super(parent, invokingState);
			this.ret = ret;
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_foreach_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterForeach_stm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitForeach_stm(this);
		}
	}

	public final Foreach_stmContext foreach_stm(Type ret,Env top) throws RecognitionException {
		Foreach_stmContext _localctx = new Foreach_stmContext(_ctx, getState(), ret, top);
		enterRule(_localctx, 64, RULE_foreach_stm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(485);
			match(T__37);
			setState(486);
			match(T__2);
			setState(487);
			((Foreach_stmContext)_localctx).type = type();
			setState(488);
			((Foreach_stmContext)_localctx).ID = match(ID);
			_localctx.top.put((((Foreach_stmContext)_localctx).ID!=null?((Foreach_stmContext)_localctx).ID.getText():null), ((Foreach_stmContext)_localctx).type.t);
			setState(490);
			match(T__38);
			setState(491);
			((Foreach_stmContext)_localctx).expr = expr(_localctx.top);
			((Foreach_stmContext)_localctx).i =  0; ((Foreach_stmContext)_localctx).size =  ((Foreach_stmContext)_localctx).expr.t.size/4;
			setState(493);
			match(T__5);
			setState(494);
			statement(_localctx.ret, _localctx.top);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Return_stmContext extends ParserRuleContext {
		public Type ret;
		public Env top;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Return_stmContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Return_stmContext(ParserRuleContext parent, int invokingState, Type ret, Env top) {
			super(parent, invokingState);
			this.ret = ret;
			this.top = top;
		}
		@Override public int getRuleIndex() { return RULE_return_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterReturn_stm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitReturn_stm(this);
		}
	}

	public final Return_stmContext return_stm(Type ret,Env top) throws RecognitionException {
		Return_stmContext _localctx = new Return_stmContext(_ctx, getState(), ret, top);
		enterRule(_localctx, 66, RULE_return_stm);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(496);
			match(T__39);
			setState(498);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__9) | (1L << T__30) | (1L << T__32) | (1L << T__35) | (1L << CONST_NUM) | (1L << CONST_CHAR) | (1L << CONST_STR) | (1L << CONST_FUNC) | (1L << ID))) != 0)) {
				{
				setState(497);
				expr(_localctx.top);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Break_stmContext extends ParserRuleContext {
		public Break_stmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_break_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).enterBreak_stm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof XListener ) ((XListener)listener).exitBreak_stm(this);
		}
	}

	public final Break_stmContext break_stm() throws RecognitionException {
		Break_stmContext _localctx = new Break_stmContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_break_stm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(500);
			match(T__40);

			        isBreak = true;
			        breakAdr.add(pc);
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\63\u01fa\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\6"+
		"\2T\n\2\r\2\16\2U\3\2\3\2\3\3\3\3\3\3\3\3\5\3^\n\3\3\4\3\4\3\4\3\4\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\5\5j\n\5\3\5\3\5\3\5\3\5\3\5\5\5q\n\5\3\5\3\5\3\5"+
		"\7\5v\n\5\f\5\16\5y\13\5\5\5{\n\5\3\5\3\5\3\6\3\6\3\6\3\6\5\6\u0083\n"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u008b\n\6\3\6\3\6\3\6\3\6\3\6\7\6\u0092"+
		"\n\6\f\6\16\6\u0095\13\6\5\6\u0097\n\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7"+
		"\3\b\3\b\3\b\3\b\3\b\5\b\u00a6\n\b\3\b\3\b\3\b\3\b\3\b\5\b\u00ad\n\b\7"+
		"\b\u00af\n\b\f\b\16\b\u00b2\13\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00bb"+
		"\n\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00c3\n\t\3\t\3\t\3\t\7\t\u00c8\n\t\f"+
		"\t\16\t\u00cb\13\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\7\t\u00de\n\t\f\t\16\t\u00e1\13\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00f4\n\t\3\n\3\n"+
		"\3\n\7\n\u00f9\n\n\f\n\16\n\u00fc\13\n\3\n\3\n\3\n\3\13\3\13\5\13\u0103"+
		"\n\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f"+
		"\u0114\n\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r"+
		"\3\r\3\r\3\r\3\r\5\r\u0129\n\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\5\16\u0137\n\16\3\17\3\17\3\17\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\5\20\u0144\n\20\3\21\3\21\3\21\3\21\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\5\22\u0150\n\22\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\5\24\u015c\n\24\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\5\26\u0168\n\26\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30"+
		"\5\30\u0174\n\30\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\5\32"+
		"\u0180\n\32\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\5\34\u018c"+
		"\n\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\5\35\u0195\n\35\3\36\3\36\3\36"+
		"\3\36\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\7\37\u01aa\n\37\f\37\16\37\u01ad\13\37\5\37\u01af\n\37\3\37"+
		"\5\37\u01b2\n\37\3\37\3\37\3\37\5\37\u01b7\n\37\3 \3 \3 \3 \3 \3 \3 \3"+
		" \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \7 \u01cc\n \f \16 \u01cf\13 \5 \u01d1"+
		"\n \3 \3 \3 \3 \3 \3 \3 \3 \3 \5 \u01dc\n \3!\3!\3!\3!\3!\3!\3!\3!\3!"+
		"\3!\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3#\3#\5#\u01f5\n#\3$\3"+
		"$\3$\3$\2\2%\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64"+
		"\668:<>@BDF\2\7\3\2\36\37\4\2\20\20\23\23\3\2 !\4\2\21\21\"\"\4\2!!##"+
		"\u0212\2H\3\2\2\2\4]\3\2\2\2\6_\3\2\2\2\bc\3\2\2\2\n~\3\2\2\2\f\u009b"+
		"\3\2\2\2\16\u00a0\3\2\2\2\20\u00f3\3\2\2\2\22\u00f5\3\2\2\2\24\u0102\3"+
		"\2\2\2\26\u0113\3\2\2\2\30\u0128\3\2\2\2\32\u0136\3\2\2\2\34\u0138\3\2"+
		"\2\2\36\u0143\3\2\2\2 \u0145\3\2\2\2\"\u014f\3\2\2\2$\u0151\3\2\2\2&\u015b"+
		"\3\2\2\2(\u015d\3\2\2\2*\u0167\3\2\2\2,\u0169\3\2\2\2.\u0173\3\2\2\2\60"+
		"\u0175\3\2\2\2\62\u017f\3\2\2\2\64\u0181\3\2\2\2\66\u018b\3\2\2\28\u0194"+
		"\3\2\2\2:\u0196\3\2\2\2<\u01b6\3\2\2\2>\u01b8\3\2\2\2@\u01dd\3\2\2\2B"+
		"\u01e7\3\2\2\2D\u01f2\3\2\2\2F\u01f6\3\2\2\2HS\b\2\1\2IJ\5\4\3\2JK\7\3"+
		"\2\2KT\3\2\2\2LT\5\n\6\2MN\5\f\7\2NO\7\3\2\2OT\3\2\2\2PQ\5\16\b\2QR\7"+
		"\3\2\2RT\3\2\2\2SI\3\2\2\2SL\3\2\2\2SM\3\2\2\2SP\3\2\2\2TU\3\2\2\2US\3"+
		"\2\2\2UV\3\2\2\2VW\3\2\2\2WX\b\2\1\2X\3\3\2\2\2YZ\5\b\5\2Z[\b\3\1\2[^"+
		"\3\2\2\2\\^\5\6\4\2]Y\3\2\2\2]\\\3\2\2\2^\5\3\2\2\2_`\7\4\2\2`a\7\60\2"+
		"\2ab\b\4\1\2b\7\3\2\2\2cd\5\20\t\2de\7\60\2\2ef\7\5\2\2fz\b\5\1\2gh\7"+
		"\60\2\2hj\7\6\2\2ig\3\2\2\2ij\3\2\2\2jk\3\2\2\2kl\5\20\t\2lw\b\5\1\2m"+
		"p\7\7\2\2no\7\60\2\2oq\7\6\2\2pn\3\2\2\2pq\3\2\2\2qr\3\2\2\2rs\5\20\t"+
		"\2st\b\5\1\2tv\3\2\2\2um\3\2\2\2vy\3\2\2\2wu\3\2\2\2wx\3\2\2\2x{\3\2\2"+
		"\2yw\3\2\2\2zi\3\2\2\2z{\3\2\2\2{|\3\2\2\2|}\7\b\2\2}\t\3\2\2\2~\177\5"+
		"\20\t\2\177\u0080\7\60\2\2\u0080\u0096\7\5\2\2\u0081\u0083\7\t\2\2\u0082"+
		"\u0081\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0084\3\2\2\2\u0084\u0085\7\60"+
		"\2\2\u0085\u0086\7\6\2\2\u0086\u0087\5\20\t\2\u0087\u0093\b\6\1\2\u0088"+
		"\u008a\7\7\2\2\u0089\u008b\7\t\2\2\u008a\u0089\3\2\2\2\u008a\u008b\3\2"+
		"\2\2\u008b\u008c\3\2\2\2\u008c\u008d\7\60\2\2\u008d\u008e\7\6\2\2\u008e"+
		"\u008f\5\20\t\2\u008f\u0090\b\6\1\2\u0090\u0092\3\2\2\2\u0091\u0088\3"+
		"\2\2\2\u0092\u0095\3\2\2\2\u0093\u0091\3\2\2\2\u0093\u0094\3\2\2\2\u0094"+
		"\u0097\3\2\2\2\u0095\u0093\3\2\2\2\u0096\u0082\3\2\2\2\u0096\u0097\3\2"+
		"\2\2\u0097\u0098\3\2\2\2\u0098\u0099\7\b\2\2\u0099\u009a\5\22\n\2\u009a"+
		"\13\3\2\2\2\u009b\u009c\5\6\4\2\u009c\u009d\7\n\2\2\u009d\u009e\5\20\t"+
		"\2\u009e\u009f\b\7\1\2\u009f\r\3\2\2\2\u00a0\u00a1\5\20\t\2\u00a1\u00a2"+
		"\7\60\2\2\u00a2\u00a5\b\b\1\2\u00a3\u00a4\7\13\2\2\u00a4\u00a6\5\34\17"+
		"\2\u00a5\u00a3\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6\u00b0\3\2\2\2\u00a7\u00a8"+
		"\7\7\2\2\u00a8\u00a9\7\60\2\2\u00a9\u00ac\b\b\1\2\u00aa\u00ab\7\13\2\2"+
		"\u00ab\u00ad\5\34\17\2\u00ac\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad\u00af"+
		"\3\2\2\2\u00ae\u00a7\3\2\2\2\u00af\u00b2\3\2\2\2\u00b0\u00ae\3\2\2\2\u00b0"+
		"\u00b1\3\2\2\2\u00b1\17\3\2\2\2\u00b2\u00b0\3\2\2\2\u00b3\u00b4\7\60\2"+
		"\2\u00b4\u00f4\b\t\1\2\u00b5\u00b6\b\t\1\2\u00b6\u00ba\7\f\2\2\u00b7\u00b8"+
		"\7\60\2\2\u00b8\u00b9\b\t\1\2\u00b9\u00bb\7\6\2\2\u00ba\u00b7\3\2\2\2"+
		"\u00ba\u00bb\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc\u00bd\5\20\t\2\u00bd\u00c9"+
		"\b\t\1\2\u00be\u00c2\7\7\2\2\u00bf\u00c0\7\60\2\2\u00c0\u00c1\b\t\1\2"+
		"\u00c1\u00c3\7\6\2\2\u00c2\u00bf\3\2\2\2\u00c2\u00c3\3\2\2\2\u00c3\u00c4"+
		"\3\2\2\2\u00c4\u00c5\5\20\t\2\u00c5\u00c6\b\t\1\2\u00c6\u00c8\3\2\2\2"+
		"\u00c7\u00be\3\2\2\2\u00c8\u00cb\3\2\2\2\u00c9\u00c7\3\2\2\2\u00c9\u00ca"+
		"\3\2\2\2\u00ca\u00cc\3\2\2\2\u00cb\u00c9\3\2\2\2\u00cc\u00cd\7\r\2\2\u00cd"+
		"\u00f4\3\2\2\2\u00ce\u00cf\7\16\2\2\u00cf\u00d0\7\5\2\2\u00d0\u00d1\7"+
		",\2\2\u00d1\u00d2\7\b\2\2\u00d2\u00d3\7\17\2\2\u00d3\u00d4\5\20\t\2\u00d4"+
		"\u00d5\b\t\1\2\u00d5\u00f4\3\2\2\2\u00d6\u00d7\7\20\2\2\u00d7\u00d8\5"+
		"\20\t\2\u00d8\u00df\b\t\1\2\u00d9\u00da\7\21\2\2\u00da\u00db\5\20\t\2"+
		"\u00db\u00dc\b\t\1\2\u00dc\u00de\3\2\2\2\u00dd\u00d9\3\2\2\2\u00de\u00e1"+
		"\3\2\2\2\u00df\u00dd\3\2\2\2\u00df\u00e0\3\2\2\2\u00e0\u00e2\3\2\2\2\u00e1"+
		"\u00df\3\2\2\2\u00e2\u00e3\b\t\1\2\u00e3\u00e4\7\22\2\2\u00e4\u00e5\5"+
		"\20\t\2\u00e5\u00e6\b\t\1\2\u00e6\u00e7\7\23\2\2\u00e7\u00f4\3\2\2\2\u00e8"+
		"\u00e9\7\24\2\2\u00e9\u00ea\7\5\2\2\u00ea\u00eb\7,\2\2\u00eb\u00ec\7\b"+
		"\2\2\u00ec\u00f4\b\t\1\2\u00ed\u00ee\7\25\2\2\u00ee\u00f4\b\t\1\2\u00ef"+
		"\u00f0\7\26\2\2\u00f0\u00f4\b\t\1\2\u00f1\u00f2\7\27\2\2\u00f2\u00f4\b"+
		"\t\1\2\u00f3\u00b3\3\2\2\2\u00f3\u00b5\3\2\2\2\u00f3\u00ce\3\2\2\2\u00f3"+
		"\u00d6\3\2\2\2\u00f3\u00e8\3\2\2\2\u00f3\u00ed\3\2\2\2\u00f3\u00ef\3\2"+
		"\2\2\u00f3\u00f1\3\2\2\2\u00f4\21\3\2\2\2\u00f5\u00f6\7\30\2\2\u00f6\u00fa"+
		"\b\n\1\2\u00f7\u00f9\5\24\13\2\u00f8\u00f7\3\2\2\2\u00f9\u00fc\3\2\2\2"+
		"\u00fa\u00f8\3\2\2\2\u00fa\u00fb\3\2\2\2\u00fb\u00fd\3\2\2\2\u00fc\u00fa"+
		"\3\2\2\2\u00fd\u00fe\b\n\1\2\u00fe\u00ff\7\31\2\2\u00ff\23\3\2\2\2\u0100"+
		"\u0103\5\30\r\2\u0101\u0103\5\26\f\2\u0102\u0100\3\2\2\2\u0102\u0101\3"+
		"\2\2\2\u0103\25\3\2\2\2\u0104\u0105\5\16\b\2\u0105\u0106\7\3\2\2\u0106"+
		"\u0114\3\2\2\2\u0107\u0108\5\34\17\2\u0108\u0109\7\3\2\2\u0109\u0114\3"+
		"\2\2\2\u010a\u010b\5D#\2\u010b\u010c\7\3\2\2\u010c\u0114\3\2\2\2\u010d"+
		"\u0114\5\22\n\2\u010e\u0114\5@!\2\u010f\u0114\5B\"\2\u0110\u0111\5F$\2"+
		"\u0111\u0112\7\3\2\2\u0112\u0114\3\2\2\2\u0113\u0104\3\2\2\2\u0113\u0107"+
		"\3\2\2\2\u0113\u010a\3\2\2\2\u0113\u010d\3\2\2\2\u0113\u010e\3\2\2\2\u0113"+
		"\u010f\3\2\2\2\u0113\u0110\3\2\2\2\u0114\27\3\2\2\2\u0115\u0116\7\32\2"+
		"\2\u0116\u0117\7\5\2\2\u0117\u0118\5\34\17\2\u0118\u0119\7\b\2\2\u0119"+
		"\u011a\b\r\1\2\u011a\u011b\5\24\13\2\u011b\u011c\b\r\1\2\u011c\u0129\3"+
		"\2\2\2\u011d\u011e\7\32\2\2\u011e\u011f\7\5\2\2\u011f\u0120\5\34\17\2"+
		"\u0120\u0121\7\b\2\2\u0121\u0122\b\r\1\2\u0122\u0123\5\32\16\2\u0123\u0124"+
		"\7\33\2\2\u0124\u0125\b\r\1\2\u0125\u0126\5\24\13\2\u0126\u0127\b\r\1"+
		"\2\u0127\u0129\3\2\2\2\u0128\u0115\3\2\2\2\u0128\u011d\3\2\2\2\u0129\31"+
		"\3\2\2\2\u012a\u012b\7\32\2\2\u012b\u012c\7\5\2\2\u012c\u012d\5\34\17"+
		"\2\u012d\u012e\7\b\2\2\u012e\u012f\b\16\1\2\u012f\u0130\5\32\16\2\u0130"+
		"\u0131\7\33\2\2\u0131\u0132\b\16\1\2\u0132\u0133\5\32\16\2\u0133\u0134"+
		"\b\16\1\2\u0134\u0137\3\2\2\2\u0135\u0137\5\26\f\2\u0136\u012a\3\2\2\2"+
		"\u0136\u0135\3\2\2\2\u0137\33\3\2\2\2\u0138\u0139\5\36\20\2\u0139\u013a"+
		"\b\17\1\2\u013a\35\3\2\2\2\u013b\u013c\5 \21\2\u013c\u013d\7\13\2\2\u013d"+
		"\u013e\5\36\20\2\u013e\u013f\b\20\1\2\u013f\u0144\3\2\2\2\u0140\u0141"+
		"\5 \21\2\u0141\u0142\b\20\1\2\u0142\u0144\3\2\2\2\u0143\u013b\3\2\2\2"+
		"\u0143\u0140\3\2\2\2\u0144\37\3\2\2\2\u0145\u0146\5$\23\2\u0146\u0147"+
		"\5\"\22\2\u0147\u0148\b\21\1\2\u0148!\3\2\2\2\u0149\u014a\7\34\2\2\u014a"+
		"\u014b\5$\23\2\u014b\u014c\b\22\1\2\u014c\u014d\5\"\22\2\u014d\u0150\3"+
		"\2\2\2\u014e\u0150\b\22\1\2\u014f\u0149\3\2\2\2\u014f\u014e\3\2\2\2\u0150"+
		"#\3\2\2\2\u0151\u0152\5(\25\2\u0152\u0153\5&\24\2\u0153\u0154\b\23\1\2"+
		"\u0154%\3\2\2\2\u0155\u0156\7\35\2\2\u0156\u0157\5(\25\2\u0157\u0158\b"+
		"\24\1\2\u0158\u0159\5&\24\2\u0159\u015c\3\2\2\2\u015a\u015c\b\24\1\2\u015b"+
		"\u0155\3\2\2\2\u015b\u015a\3\2\2\2\u015c\'\3\2\2\2\u015d\u015e\5,\27\2"+
		"\u015e\u015f\5*\26\2\u015f\u0160\b\25\1\2\u0160)\3\2\2\2\u0161\u0162\t"+
		"\2\2\2\u0162\u0163\5,\27\2\u0163\u0164\b\26\1\2\u0164\u0165\5*\26\2\u0165"+
		"\u0168\3\2\2\2\u0166\u0168\b\26\1\2\u0167\u0161\3\2\2\2\u0167\u0166\3"+
		"\2\2\2\u0168+\3\2\2\2\u0169\u016a\5\60\31\2\u016a\u016b\5.\30\2\u016b"+
		"\u016c\b\27\1\2\u016c-\3\2\2\2\u016d\u016e\t\3\2\2\u016e\u016f\5\60\31"+
		"\2\u016f\u0170\b\30\1\2\u0170\u0171\5.\30\2\u0171\u0174\3\2\2\2\u0172"+
		"\u0174\b\30\1\2\u0173\u016d\3\2\2\2\u0173\u0172\3\2\2\2\u0174/\3\2\2\2"+
		"\u0175\u0176\5\64\33\2\u0176\u0177\5\62\32\2\u0177\u0178\b\31\1\2\u0178"+
		"\61\3\2\2\2\u0179\u017a\t\4\2\2\u017a\u017b\5\64\33\2\u017b\u017c\b\32"+
		"\1\2\u017c\u017d\5\62\32\2\u017d\u0180\3\2\2\2\u017e\u0180\b\32\1\2\u017f"+
		"\u0179\3\2\2\2\u017f\u017e\3\2\2\2\u0180\63\3\2\2\2\u0181\u0182\58\35"+
		"\2\u0182\u0183\5\66\34\2\u0183\u0184\b\33\1\2\u0184\65\3\2\2\2\u0185\u0186"+
		"\t\5\2\2\u0186\u0187\58\35\2\u0187\u0188\b\34\1\2\u0188\u0189\5\66\34"+
		"\2\u0189\u018c\3\2\2\2\u018a\u018c\b\34\1\2\u018b\u0185\3\2\2\2\u018b"+
		"\u018a\3\2\2\2\u018c\67\3\2\2\2\u018d\u018e\t\6\2\2\u018e\u018f\58\35"+
		"\2\u018f\u0190\b\35\1\2\u0190\u0195\3\2\2\2\u0191\u0192\5:\36\2\u0192"+
		"\u0193\b\35\1\2\u0193\u0195\3\2\2\2\u0194\u018d\3\2\2\2\u0194\u0191\3"+
		"\2\2\2\u01959\3\2\2\2\u0196\u0197\5> \2\u0197\u0198\5<\37\2\u0198\u0199"+
		"\b\36\1\2\u0199;\3\2\2\2\u019a\u01b1\b\37\1\2\u019b\u019c\7$\2\2\u019c"+
		"\u019d\5> \2\u019d\u019e\b\37\1\2\u019e\u01b2\3\2\2\2\u019f\u01a0\7%\2"+
		"\2\u01a0\u01a1\7\60\2\2\u01a1\u01b2\b\37\1\2\u01a2\u01ae\7\5\2\2\u01a3"+
		"\u01a4\5\34\17\2\u01a4\u01ab\b\37\1\2\u01a5\u01a6\7\7\2\2\u01a6\u01a7"+
		"\5\34\17\2\u01a7\u01a8\b\37\1\2\u01a8\u01aa\3\2\2\2\u01a9\u01a5\3\2\2"+
		"\2\u01aa\u01ad\3\2\2\2\u01ab\u01a9\3\2\2\2\u01ab\u01ac\3\2\2\2\u01ac\u01af"+
		"\3\2\2\2\u01ad\u01ab\3\2\2\2\u01ae\u01a3\3\2\2\2\u01ae\u01af\3\2\2\2\u01af"+
		"\u01b0\3\2\2\2\u01b0\u01b2\7\b\2\2\u01b1\u019b\3\2\2\2\u01b1\u019f\3\2"+
		"\2\2\u01b1\u01a2\3\2\2\2\u01b2\u01b3\3\2\2\2\u01b3\u01b4\b\37\1\2\u01b4"+
		"\u01b7\5<\37\2\u01b5\u01b7\3\2\2\2\u01b6\u019a\3\2\2\2\u01b6\u01b5\3\2"+
		"\2\2\u01b7=\3\2\2\2\u01b8\u01db\b \1\2\u01b9\u01ba\7,\2\2\u01ba\u01dc"+
		"\b \1\2\u01bb\u01bc\7-\2\2\u01bc\u01dc\b \1\2\u01bd\u01be\7.\2\2\u01be"+
		"\u01dc\b \1\2\u01bf\u01c0\7/\2\2\u01c0\u01dc\b \1\2\u01c1\u01c2\7\60\2"+
		"\2\u01c2\u01dc\b \1\2\u01c3\u01c4\b \1\2\u01c4\u01d0\7\f\2\2\u01c5\u01c6"+
		"\5\34\17\2\u01c6\u01cd\b \1\2\u01c7\u01c8\7\7\2\2\u01c8\u01c9\5\34\17"+
		"\2\u01c9\u01ca\b \1\2\u01ca\u01cc\3\2\2\2\u01cb\u01c7\3\2\2\2\u01cc\u01cf"+
		"\3\2\2\2\u01cd\u01cb\3\2\2\2\u01cd\u01ce\3\2\2\2\u01ce\u01d1\3\2\2\2\u01cf"+
		"\u01cd\3\2\2\2\u01d0\u01c5\3\2\2\2\u01d0\u01d1\3\2\2\2\u01d1\u01d2\3\2"+
		"\2\2\u01d2\u01d3\7\r\2\2\u01d3\u01dc\b \1\2\u01d4\u01d5\7&\2\2\u01d5\u01dc"+
		"\b \1\2\u01d6\u01d7\7\5\2\2\u01d7\u01d8\5\34\17\2\u01d8\u01d9\7\b\2\2"+
		"\u01d9\u01da\b \1\2\u01da\u01dc\3\2\2\2\u01db\u01b9\3\2\2\2\u01db\u01bb"+
		"\3\2\2\2\u01db\u01bd\3\2\2\2\u01db\u01bf\3\2\2\2\u01db\u01c1\3\2\2\2\u01db"+
		"\u01c3\3\2\2\2\u01db\u01d4\3\2\2\2\u01db\u01d6\3\2\2\2\u01dc?\3\2\2\2"+
		"\u01dd\u01de\b!\1\2\u01de\u01df\7\'\2\2\u01df\u01e0\b!\1\2\u01e0\u01e1"+
		"\7\5\2\2\u01e1\u01e2\5\34\17\2\u01e2\u01e3\b!\1\2\u01e3\u01e4\7\b\2\2"+
		"\u01e4\u01e5\5\24\13\2\u01e5\u01e6\b!\1\2\u01e6A\3\2\2\2\u01e7\u01e8\7"+
		"(\2\2\u01e8\u01e9\7\5\2\2\u01e9\u01ea\5\20\t\2\u01ea\u01eb\7\60\2\2\u01eb"+
		"\u01ec\b\"\1\2\u01ec\u01ed\7)\2\2\u01ed\u01ee\5\34\17\2\u01ee\u01ef\b"+
		"\"\1\2\u01ef\u01f0\7\b\2\2\u01f0\u01f1\5\24\13\2\u01f1C\3\2\2\2\u01f2"+
		"\u01f4\7*\2\2\u01f3\u01f5\5\34\17\2\u01f4\u01f3\3\2\2\2\u01f4\u01f5\3"+
		"\2\2\2\u01f5E\3\2\2\2\u01f6\u01f7\7+\2\2\u01f7\u01f8\b$\1\2\u01f8G\3\2"+
		"\2\2*SU]ipwz\u0082\u008a\u0093\u0096\u00a5\u00ac\u00b0\u00ba\u00c2\u00c9"+
		"\u00df\u00f3\u00fa\u0102\u0113\u0128\u0136\u0143\u014f\u015b\u0167\u0173"+
		"\u017f\u018b\u0194\u01ab\u01ae\u01b1\u01b6\u01cd\u01d0\u01db\u01f4";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}