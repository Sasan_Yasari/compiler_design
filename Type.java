import java.util.ArrayList;
public class Type{
        String name;
        ArrayList<Pair<String, Type>> t;
        int size;
        Type(String name){
            this.name = name;
            this.t = new ArrayList<Pair<String, Type>>();
            if(name.equals("int") || name.equals("char"))
                this.size = 4;
            else 
                this.size = 0;
        } 
        Type(Type t){
            this.name = t.name;
            this.t = new ArrayList<Pair<String, Type>>();
            for(int i =0;i<t.t.size();i++){
                Pair<String, Type> newPair = new Pair(t.t.get(i).getL(), t.t.get(i).getR());
                this.t.add(newPair);
            }
        } 
        public boolean sameType(){
            for(int i=0;i<this.t.size();i++)
                if(!this.t.get(i).getR().equal(this.t.get(0).getR()))
                    return false;
            return true;
        }
		public void addType(String name, Type t){
            Pair<String, Type> newPair = new Pair(name, t);
            this.t.add(newPair);  
            this.size += t.size;      
        }
        public void printType(){
            System.out.print(this.name+" ");
            for(int i=0;i<this.t.size();i++){
                if(this.t.get(i).getR()!=null){
                    if(this.t.get(i).getR().name.equals("list") || this.t.get(i).getR().name.equals("ptf")){
                        System.out.print(" ");
                        this.t.get(i).getR().printType();
                    }
                    else
                        System.out.print("  "+this.t.get(i).getR().name+" "); 
                }  
                else 
                    System.out.print("  ->");
            }
        }
        public Type contain(String typeName){
            for(int i=0;i<this.t.size();i++){
                if(this.t.get(i).getL().equals(typeName))
                    return this.t.get(i).getR();
                else if(this.t.get(i).getL().equals("list"))
                    return this.t.get(i).getR().contain(typeName);
            }   
            return null;
        }
        public boolean equal(Type t){
            if(!this.name.equals(t.name))
                return false;
            if(this.name.equals("int") || this.name.equals("char"))
                return true;
            else{
                if(this.t.size() != t.t.size())
                    return false;
                for(int i=0;i<this.t.size();i++){
                    if(!this.t.get(i).getR().equal(t.t.get(i).getR()))
                        return false;
                }
            }
            return true;
        }
		public void print(String str){
			System.out.println(str);
		}
}
