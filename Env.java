import java.util.ArrayList;

public class Env{
    Env prev;
    ArrayList<Pair3<String, Type, Integer>> st;
    int offset;
    Env(Env prev){
        this.prev = prev;
        this.st = new ArrayList<Pair3<String, Type, Integer>>();
        this.offset = 0;
    }
    public void put(String name, Type t){
		Pair3 newPair = new Pair3(name, t, this.offset);
		this.st.add(newPair);
		this.offset += t.size;
	}
	public int getSize(String name){
		Env temp = this;
		while(temp != null){
			for(int i=0; i<temp.st.size(); i++)
				if(temp.st.get(i).getF().equals(name))
					return temp.st.get(i).getS().size;
			temp = temp.prev;
		}
		return -1;
	}
	public int getAdr(String name){
		int ret = -1;
		boolean find = false;
		Env temp = this;
		while(temp != null){
			for(int i=0; i<temp.st.size(); i++)
				if(temp.st.get(i).getF().equals(name)){
					ret = temp.st.get(i).getT();
					find = true;
				}
			temp = temp.prev;
			if(find)
				break;
		}
		while(temp != null){
			ret += temp.offset;
			temp = temp.prev;
		}
		return ret;
	}
	public void printEnv(){
		for(int i=0;i<this.st.size();i++){
			System.out.print(this.st.get(i).getF()+" "+this.st.get(i).getT()+": ");
			this.st.get(i).getS().printType();
			print("");
		}
		print("offset: "+this.offset);
	}
	public void print(String str){
		System.out.println(str);
	}
}