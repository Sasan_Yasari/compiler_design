import java.util.ArrayList;
import java.util.HashMap;

public class X{
	public ArrayList<Env> envs;
    
    public X(){
    	this.envs = new ArrayList<Env>();
    }
	public Env addEnv(Env prev){
    	Env newEnv = new Env(prev);
    	this.envs.add(newEnv);
    	return newEnv;
	}
	public void printEnvs(){
		print("\nscopes information:");
		for(int i = 0;i < this.envs.size(); i++){
			print(i+":\n{");
			this.envs.get(i).printEnv();	
			print("}");
		}
	}
	public Type checkVar(Env top, String name){
		Env cur = top;
		while(cur != null){
			for(int i=0; i<cur.st.size(); i++)
				if(cur.st.get(i).getF().equals(name))
					return cur.st.get(i).getS();
			cur = cur.prev;
		}
		return null;
	}
	public Type getType(Env top, String name){
		Env cur = top;
		while(cur != null){
			for(int i=0; i<cur.st.size(); i++)
				if(cur.st.get(i).getF().equals(name))
					return cur.st.get(i).getS();
			cur = cur.prev;
		}
		return null;
	}
	public void print(String str){
		System.out.println(str);
	}
}
