// Generated from X.g4 by ANTLR 4.5

    import java.util.Vector;
    import java.util.Random;
    import java.util.ArrayList;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link XParser}.
 */
public interface XListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link XParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(XParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(XParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#fordecl}.
	 * @param ctx the parse tree
	 */
	void enterFordecl(XParser.FordeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#fordecl}.
	 * @param ctx the parse tree
	 */
	void exitFordecl(XParser.FordeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#type_fordecl}.
	 * @param ctx the parse tree
	 */
	void enterType_fordecl(XParser.Type_fordeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#type_fordecl}.
	 * @param ctx the parse tree
	 */
	void exitType_fordecl(XParser.Type_fordeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#func_fordecl}.
	 * @param ctx the parse tree
	 */
	void enterFunc_fordecl(XParser.Func_fordeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#func_fordecl}.
	 * @param ctx the parse tree
	 */
	void exitFunc_fordecl(XParser.Func_fordeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#funcdef}.
	 * @param ctx the parse tree
	 */
	void enterFuncdef(XParser.FuncdefContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#funcdef}.
	 * @param ctx the parse tree
	 */
	void exitFuncdef(XParser.FuncdefContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#typedef}.
	 * @param ctx the parse tree
	 */
	void enterTypedef(XParser.TypedefContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#typedef}.
	 * @param ctx the parse tree
	 */
	void exitTypedef(XParser.TypedefContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#vardef}.
	 * @param ctx the parse tree
	 */
	void enterVardef(XParser.VardefContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#vardef}.
	 * @param ctx the parse tree
	 */
	void exitVardef(XParser.VardefContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(XParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(XParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(XParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(XParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(XParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(XParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#other_stm}.
	 * @param ctx the parse tree
	 */
	void enterOther_stm(XParser.Other_stmContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#other_stm}.
	 * @param ctx the parse tree
	 */
	void exitOther_stm(XParser.Other_stmContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#if_stm}.
	 * @param ctx the parse tree
	 */
	void enterIf_stm(XParser.If_stmContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#if_stm}.
	 * @param ctx the parse tree
	 */
	void exitIf_stm(XParser.If_stmContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#if_stm_else}.
	 * @param ctx the parse tree
	 */
	void enterIf_stm_else(XParser.If_stm_elseContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#if_stm_else}.
	 * @param ctx the parse tree
	 */
	void exitIf_stm_else(XParser.If_stm_elseContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(XParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(XParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_assign}.
	 * @param ctx the parse tree
	 */
	void enterExpr_assign(XParser.Expr_assignContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_assign}.
	 * @param ctx the parse tree
	 */
	void exitExpr_assign(XParser.Expr_assignContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_or}.
	 * @param ctx the parse tree
	 */
	void enterExpr_or(XParser.Expr_orContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_or}.
	 * @param ctx the parse tree
	 */
	void exitExpr_or(XParser.Expr_orContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_or_tmp}.
	 * @param ctx the parse tree
	 */
	void enterExpr_or_tmp(XParser.Expr_or_tmpContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_or_tmp}.
	 * @param ctx the parse tree
	 */
	void exitExpr_or_tmp(XParser.Expr_or_tmpContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_and}.
	 * @param ctx the parse tree
	 */
	void enterExpr_and(XParser.Expr_andContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_and}.
	 * @param ctx the parse tree
	 */
	void exitExpr_and(XParser.Expr_andContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_and_tmp}.
	 * @param ctx the parse tree
	 */
	void enterExpr_and_tmp(XParser.Expr_and_tmpContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_and_tmp}.
	 * @param ctx the parse tree
	 */
	void exitExpr_and_tmp(XParser.Expr_and_tmpContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_eq}.
	 * @param ctx the parse tree
	 */
	void enterExpr_eq(XParser.Expr_eqContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_eq}.
	 * @param ctx the parse tree
	 */
	void exitExpr_eq(XParser.Expr_eqContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_eq_tmp}.
	 * @param ctx the parse tree
	 */
	void enterExpr_eq_tmp(XParser.Expr_eq_tmpContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_eq_tmp}.
	 * @param ctx the parse tree
	 */
	void exitExpr_eq_tmp(XParser.Expr_eq_tmpContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_cmp}.
	 * @param ctx the parse tree
	 */
	void enterExpr_cmp(XParser.Expr_cmpContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_cmp}.
	 * @param ctx the parse tree
	 */
	void exitExpr_cmp(XParser.Expr_cmpContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_cmp_tmp}.
	 * @param ctx the parse tree
	 */
	void enterExpr_cmp_tmp(XParser.Expr_cmp_tmpContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_cmp_tmp}.
	 * @param ctx the parse tree
	 */
	void exitExpr_cmp_tmp(XParser.Expr_cmp_tmpContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_add}.
	 * @param ctx the parse tree
	 */
	void enterExpr_add(XParser.Expr_addContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_add}.
	 * @param ctx the parse tree
	 */
	void exitExpr_add(XParser.Expr_addContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_add_tmp}.
	 * @param ctx the parse tree
	 */
	void enterExpr_add_tmp(XParser.Expr_add_tmpContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_add_tmp}.
	 * @param ctx the parse tree
	 */
	void exitExpr_add_tmp(XParser.Expr_add_tmpContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_mult}.
	 * @param ctx the parse tree
	 */
	void enterExpr_mult(XParser.Expr_multContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_mult}.
	 * @param ctx the parse tree
	 */
	void exitExpr_mult(XParser.Expr_multContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_mult_tmp}.
	 * @param ctx the parse tree
	 */
	void enterExpr_mult_tmp(XParser.Expr_mult_tmpContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_mult_tmp}.
	 * @param ctx the parse tree
	 */
	void exitExpr_mult_tmp(XParser.Expr_mult_tmpContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_un}.
	 * @param ctx the parse tree
	 */
	void enterExpr_un(XParser.Expr_unContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_un}.
	 * @param ctx the parse tree
	 */
	void exitExpr_un(XParser.Expr_unContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_mem}.
	 * @param ctx the parse tree
	 */
	void enterExpr_mem(XParser.Expr_memContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_mem}.
	 * @param ctx the parse tree
	 */
	void exitExpr_mem(XParser.Expr_memContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_mem_tmp}.
	 * @param ctx the parse tree
	 */
	void enterExpr_mem_tmp(XParser.Expr_mem_tmpContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_mem_tmp}.
	 * @param ctx the parse tree
	 */
	void exitExpr_mem_tmp(XParser.Expr_mem_tmpContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#expr_other}.
	 * @param ctx the parse tree
	 */
	void enterExpr_other(XParser.Expr_otherContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#expr_other}.
	 * @param ctx the parse tree
	 */
	void exitExpr_other(XParser.Expr_otherContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#while_stm}.
	 * @param ctx the parse tree
	 */
	void enterWhile_stm(XParser.While_stmContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#while_stm}.
	 * @param ctx the parse tree
	 */
	void exitWhile_stm(XParser.While_stmContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#foreach_stm}.
	 * @param ctx the parse tree
	 */
	void enterForeach_stm(XParser.Foreach_stmContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#foreach_stm}.
	 * @param ctx the parse tree
	 */
	void exitForeach_stm(XParser.Foreach_stmContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#return_stm}.
	 * @param ctx the parse tree
	 */
	void enterReturn_stm(XParser.Return_stmContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#return_stm}.
	 * @param ctx the parse tree
	 */
	void exitReturn_stm(XParser.Return_stmContext ctx);
	/**
	 * Enter a parse tree produced by {@link XParser#break_stm}.
	 * @param ctx the parse tree
	 */
	void enterBreak_stm(XParser.Break_stmContext ctx);
	/**
	 * Exit a parse tree produced by {@link XParser#break_stm}.
	 * @param ctx the parse tree
	 */
	void exitBreak_stm(XParser.Break_stmContext ctx);
}