public class Pair3<F,S,T>{
    private F f;
    private S s;
    private T t;
    Pair3(F f, S s, T t){
        this.f = f;
        this.s = s;
        this.t = t;
    }
    public F getF(){ return f; }
    public S getS(){ return s; }
    public T getT(){ return t; }
}